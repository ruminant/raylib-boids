#include <vector>
#include "raylib.h"
#include "tea/math.h"

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"                 // Required for GUI controls


Vector2 as_Vector2(const tea::Vec2 &v) { Vector2 O; O.x = v.x; O.y = v.y; return O;}
tea::Vec2 as_Vec2(const Vector2 &v) { tea::Vec2 O; O.x = v.x; O.y = v.y; return O;}
Rectangle as_Rect(float x, float y, float w, float h) { Rectangle R; R.x = x; R.y = y; R.width = w; R.height = h; return R;}
struct Boid
{
    static int  uid;
    int         id;
    tea::Vec2   position;
    tea::Vec2   velocity;
    Color       colour=BLUE;
    float       radius=8.f;

    Boid()
    {
        id = uid++;
        position.x = (float)GetRandomValue(0, GetScreenWidth());
        position.y = (float)GetRandomValue(0, GetScreenHeight());
    }

    void Draw(float x, float y, float r, Color col) const
    {
        DrawCircle((int)x, (int)y, r, col);
    }

    void Draw() const
    {
        DrawCircle((int)position.x, (int)position.y, radius, colour);
    }

    bool operator==(const Boid &other) const
    {
        return id == other.id;
    }
};
int Boid::uid = 0;

class BoidFlock
{
    float scale = 1.f;
public:
    float CentreOfMassFactor = 0.01f;
    float MaxSpeed = 100.f;
    float MinDistance = 100.f;
    float MatchingFactor = 1.f/8.f;
    float GoalFactor = 1.f/100.f;
    BoidFlock(int num, float scaleIn=1.0f)
    {
        boids.resize(num);
        scale = scaleIn;
    }

    void InitialisePositions()
    {
        // TODO: Setup Positions
    }

    void DrawBoids()
    {
        for(const auto &boid : boids )
        {
            boid.Draw(
                boid.position.x*scale,
                boid.position.y*scale,
                boid.radius, boid.colour);
        }
    }

    void UpdateBoids()
    {
        tea::Vec2 goal = as_Vec2(GetMousePosition()) / scale;
        
        for(auto &boid : boids)
        {
            tea::Vec2 v1 = Rule1(boid);
            tea::Vec2 v2 = Rule2(boid);
            tea::Vec2 v3 = Rule3(boid);
            tea::Vec2 v4 = Rule4(boid, goal);

            boid.velocity += v1 + v2 + v3 + v4;
            LimitVelocity(boid);
            boid.position += boid.velocity;

            
        }
    }

    void LimitVelocity(Boid &boid)
    {
        const float maxSpeedSqr = MaxSpeed * MaxSpeed;
        
        float speedSqr = tea::LengthSqr(boid.velocity);
        
        if (speedSqr > maxSpeedSqr)
        {
            boid.velocity = (boid.velocity / sqrtf(speedSqr)) * MaxSpeed;
        }
    }

    tea::Vec2 Rule1(Boid &boid)
    {
        // find centre of mass
        tea::Vec2 vec;
        for (auto &other : boids)
        {
            if (other == boid)
                continue;
            vec += other.position;
        }

        vec /= (float)(boids.size() - 1);

        return (vec - boid.position) * CentreOfMassFactor;
    }

    tea::Vec2 Rule2(Boid &boid)
    {
        // maintain minimum distance
        
        const float MinDistanceSqr = MinDistance * MinDistance;

        tea::Vec2 vec(0,0);

        for (auto &other: boids)
        {
            if (other == boid)
                continue;
            if (tea::DistanceSqr(other.position, boid.position) < MinDistanceSqr)
                vec = vec - (other.position - boid.position);
        }

        return vec;
    }

    tea::Vec2 Rule3(Boid &boid)
    {
        // match velocity of nearby boids
        

        tea::Vec2 vel(0.f,0.f);

        for (auto &other: boids)
        {
            if (other == boid)
                continue;
            vel += other.velocity;
        }

        vel /= (float)(boids.size()-1);

        return (vel - boid.velocity) * MatchingFactor;
    }

    tea::Vec2 Rule4(Boid &boid, const tea::Vec2 &goal)
    {

        return (goal - boid.position) * GoalFactor;

    }
protected:
    std::vector<Boid> boids;

};

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;

    InitWindow(screenWidth, screenHeight, "raylib - boids");

    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------

    BoidFlock flock(50, 0.1f);

    float CentreOfMass = flock.CentreOfMassFactor * 10000.f;
    float MaxSpeed = flock.MaxSpeed * 1.f;
    float MinDistance = flock.MinDistance * 1.f;
    float MatchingFactor = flock.MatchingFactor * 1000.f;
    float GoalFactor = flock.GoalFactor * 1000.f;
    // Main game loop
    while (!WindowShouldClose())
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(RAYWHITE);

            DrawText("Boids Example!", 190, 200, 20, LIGHTGRAY);

            // GUI
            CentreOfMass = GuiSlider(as_Rect(600, 40, 120, 20), "CentreMass", CentreOfMass, 1.0, 100.0f, true);
            MaxSpeed = GuiSlider(as_Rect(600, 80, 120, 20), "MaxSpeed", MaxSpeed, 1.0, 100.0f, true);
            MinDistance = GuiSlider(as_Rect(600, 120, 120, 20), "MinDist", MinDistance, 1.0, 1000.0f, true);
            MatchingFactor = GuiSlider(as_Rect(600, 160, 120, 20), "MatchF", MatchingFactor, 1.0, 10.0f, true);
            GoalFactor = GuiSlider(as_Rect(600, 200, 120, 20), "GoalF", GoalFactor, 1.0, 10.0f, true);

            flock.CentreOfMassFactor = CentreOfMass / 10000.f;
            flock.MaxSpeed = MaxSpeed / 1.f;
            flock.MinDistance = MinDistance / 1.f;
            flock.MatchingFactor = MatchingFactor / 1000.f;
            flock.GoalFactor = GoalFactor / 1000.f;

            flock.UpdateBoids();

            flock.DrawBoids();

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}
