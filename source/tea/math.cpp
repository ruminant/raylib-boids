#include "tea/math.h"
#include <math.h>

namespace tea {

//----------------------------------------------------------------
//  Plane
//----------------------------------------------------------------
bool GetIntersectionWithLine(const Plane &plane,
	const Vec3 &linePoint,
	const Vec3 &lineVect,
	Vec3 &outIntersection)
{
	float t2 = Dot( plane.normal, lineVect );
	if (t2 == 0)
		return false;

	float t = - (Dot( plane.normal, linePoint) + plane.distance) / t2;
	outIntersection = linePoint + (lineVect * t);
	return true;
}

float GetKnownIntersectionWithLine( const Plane &plane,
	const Vec3& linePoint1,
	const Vec3& linePoint2)
{
	float t2 = Dot( plane.normal, (linePoint2 - linePoint1));
	return - ( Dot( plane.normal, linePoint1 ) + plane.distance) / t2;
}

bool GetIntersectionWithLimitedLine(const Plane &plane, 
	const Vec3& linePoint1,
	const Vec3& linePoint2,
	Vec3& outIntersection)
{
	return (	GetIntersectionWithLine(plane, linePoint1, linePoint2 - linePoint1, outIntersection) &&
		IsBetweenPoints( outIntersection, linePoint1, linePoint2 ) );

}

IntersectionRelation ClassifyPointRelation(const Plane &plane, const Vec3& point)
{
	const float d = Dot( plane.normal, point ) + plane.distance;

	if (d < -kRoundingError)
		return kRelationBack;

	if (d > kRoundingError)
		return kRelationFront;

	return kRelationPlanar;
}

bool GetIntersectionWithPlane(const Plane &plane,
	const Plane &other,
	Vec3  &outLinePoint,
	Vec3  &outLineVect)
{
	float fn00 = Length( plane.normal );
	float fn01 = Dot( plane.normal, other.normal );
	float fn11 = Length( other.normal );
	float det = fn00*fn11 - fn01*fn01;

	if (fabsf(det) < kRoundingError )
		return false;

	det = 1.0f / det;
	float fc0 = (fn11*-plane.distance + fn01*other.distance) * det;
	float fc1 = (fn00*-other.distance + fn01*plane.distance) * det;

	outLineVect = Cross( plane.normal, other.normal );
	outLinePoint = plane.normal * fc0  + other.normal * fc1;
	return true;
}

bool GetIntersectionWithPlanes(const Plane &plane,
	const Plane &o1,
	const Plane &o2,
	Vec3 &outPoint)
{
	Vec3 linePoint, lineVect;
	if (GetIntersectionWithPlane( plane, o1, linePoint, lineVect))
		return GetIntersectionWithLine( o2, linePoint, lineVect, outPoint );
	return false;
}

bool IsFrontFacing(const Plane &plane, const Vec3 &lookDirection)
{
	const float d = Dot( plane.normal, lookDirection );
	return d <= 0.0f;
}

//----------------------------------------------------------------
//  Axis Aligned Bounding Box
//---------------------------------------------------------------
bool IntersectsWithLine(const Aabbox &box,
	const Vec3& linemiddle, 
	const Vec3& linevect,
	float halflength)
{
	const Vec3 e = GetExtent(box) * 0.5f;
	const Vec3 t = GetCentre(box) - linemiddle;

	if ((fabsf(t.x) > e.x + halflength * fabsf(linevect.x)) || 
		(fabsf(t.y) > e.y + halflength * fabsf(linevect.y)) ||
		(fabsf(t.z) > e.z + halflength * fabsf(linevect.z)) )
		return false;

	float r = e.y * fabsf(linevect.z) + e.z * fabsf(linevect.y);
	if (fabs(t.y*linevect.z - t.z*linevect.y) > r )
		return false;

	r = e.x * fabsf(linevect.z) + e.z * fabsf(linevect.x);
	if (fabsf(t.z*linevect.x - t.x*linevect.z) > r )
		return false;

	r = e.x * fabsf(linevect.y) + e.y * fabsf(linevect.x);
	if (fabsf(t.x*linevect.y - t.y*linevect.x) > r)
		return false;

	return true;
}

Aabbox Transform( const Mat4 &m1, const Aabbox &box )
{
	return Aabbox( m1 * box.minEdge, m1 * box.maxEdge );
}

//----------------------------------------------------------------
//  View Frustum
//----------------------------------------------------------------

//! returns the point which is on the far left upper corner inside the
//! the view frustum.
Vec3 GetFarLeftUp( const ViewFrustum &frustum )
{
	Vec3 p;
	GetIntersectionWithPlanes(	frustum.planes[ ViewFrustum::kFarPlane ],
		frustum.planes[ ViewFrustum::kTopPlane ],
		frustum.planes[ ViewFrustum::kLeftPlane ],
		p );
	return p;
}

//! returns the point which is on the far left bottom corner inside the
//! the view frustum.
Vec3 GetFarLeftDown( const ViewFrustum &frustum )
{
	Vec3 p;
	GetIntersectionWithPlanes(	frustum.planes[ ViewFrustum::kFarPlane ],
		frustum.planes[ ViewFrustum::kBottomPlane ],
		frustum.planes[ ViewFrustum::kLeftPlane ],
		p );
	return p;
}

//! returns the point which is on the far right top corner inside the
//! the view frustum.
Vec3 GetFarRightUp( const ViewFrustum &frustum )
{
	Vec3 p;
	GetIntersectionWithPlanes(	frustum.planes[ ViewFrustum::kFarPlane ],
		frustum.planes[ ViewFrustum::kTopPlane ],
		frustum.planes[ ViewFrustum::kRightPlane ],
		p );
	return p;
}

//! returns the point which is on the far right bottom corner inside the
//! the view frustum.
Vec3 GetFarRightDown( const ViewFrustum &frustum )
{
	Vec3 p;
	GetIntersectionWithPlanes(	frustum.planes[ ViewFrustum::kFarPlane ],
		frustum.planes[ ViewFrustum::kBottomPlane ],
		frustum.planes[ ViewFrustum::kRightPlane ],
		p );
	return p;

}

//! recalculates the bounding box member based on the planes
void RecalculateBoundingBox( ViewFrustum &frustum )
{
	Aabbox bb( frustum.camera_position );

	bb += GetFarLeftUp( frustum );
	bb += GetFarRightUp( frustum );
	bb += GetFarLeftDown( frustum );
	bb += GetFarRightDown( frustum );

	frustum.bounding_box = bb;

}

//! This constructor creates a view frustum based on a projection and/or
//! view matrix.
ViewFrustum::ViewFrustum(const Mat4& m)
{
	// left clipping plane
	planes[ kLeftPlane ].normal.x = m[0].w + m[0].x;
	planes[ kLeftPlane ].normal.y = m[1].w + m[1].x;
	planes[ kLeftPlane ].normal.z = m[2].w + m[2].x;
	planes[ kLeftPlane ].distance = m[3].w + m[3].x;

	// right clipping plane
	planes[ kRightPlane ].normal.x = m[0].w - m[0].x;
	planes[ kRightPlane ].normal.y = m[1].w - m[1].x;
	planes[ kRightPlane ].normal.z = m[2].w - m[2].x;
	planes[ kRightPlane ].distance = m[3].w - m[3].x;

	// top clipping plane
	planes[ kTopPlane ].normal.x = m[0].w - m[0].y;
	planes[ kTopPlane ].normal.y = m[1].w - m[1].y;
	planes[ kTopPlane ].normal.z = m[2].w - m[2].y;
	planes[ kTopPlane ].distance = m[3].w - m[3].y;

	// bottom clipping plane
	planes[ kBottomPlane ].normal.x = m[0].w + m[0].y;
	planes[ kBottomPlane ].normal.y = m[1].w + m[1].y;
	planes[ kBottomPlane ].normal.z = m[2].w + m[2].y;
	planes[ kBottomPlane ].distance = m[3].w + m[3].y;

	// far clipping plane
	planes[ kFarPlane ].normal.x = m[0].w - m[0].z;
	planes[ kFarPlane ].normal.y = m[1].w - m[1].z;
	planes[ kFarPlane ].normal.z = m[2].w - m[2].z;
	planes[ kFarPlane ].distance = m[3].w - m[3].z;

	// near clipping plane
	//planes[ kNearPlane ].normal.x = m[0].z;
	//planes[ kNearPlane ].normal.y = m[1].z;
	//planes[ kNearPlane ].normal.z = m[2].z;
	//planes[ kNearPlane ].distance = m[3].z;
	planes[ kNearPlane ].normal.x = m[0].w + m[0].z;
	planes[ kNearPlane ].normal.y = m[1].w + m[1].z;
	planes[ kNearPlane ].normal.z = m[2].w + m[2].z;
	planes[ kNearPlane ].distance = m[3].w + m[3].z;


	// normalize normals

	for (int i = 0; i < 6 ; ++i )
	{
		float len = (1.0f / Length(planes[i].normal) );
		planes[i].normal *= len;
		planes[i].distance *= len;
	}

	// make bounding box
	RecalculateBoundingBox(*this);

}

void TransformPlane(Plane &plane, const Mat4 &mat)
{
	Vec3 n;
	n.x = plane.normal.x * mat[0].x + plane.normal.y * mat[1].x + plane.normal.z * mat[2].x;
	n.y = plane.normal.x * mat[0].y + plane.normal.y * mat[1].y + plane.normal.z * mat[2].y;
	n.z = plane.normal.x * mat[0].z + plane.normal.y * mat[1].z + plane.normal.z * mat[2].z;

	plane.distance = mat[3].x * n.x + mat[3].y * n.y + mat[3].z * n.z;
	plane.normal.x = n.x;
	plane.normal.y = n.y;
	plane.normal.z = n.z;
}

//! transforms the furstum by the matrix
//! \param Matrix by which the view frustum is transformed.
void TransformFrustum( ViewFrustum &frustum, const Mat4 & mat)
{
	for (int i=0; i< 6 ; ++i)
		TransformPlane( frustum.planes[i], mat );

	frustum.camera_position = mat * frustum.camera_position;
	RecalculateBoundingBox( frustum );
}

//----------------------------------------------------------------
//  Mat4
//----------------------------------------------------------------
Mat4::Mat4( ZeroTag ) : x(kZero), y(kZero), z(kZero), w(kZero)
{
}

Mat4::Mat4( IdentityTag ) : x(kUnitXAxis), y(kUnitYAxis), z(kUnitZAxis), w(kUnitWAxis)
{
}

Mat4::Mat4( const Quat &q ) : w(kUnitWAxis)
{
	x.Set( 1 - 2 * q.y * q.y - 2 * q.z * q.z,     2 * q.x * q.y + 2 * q.w * q.z,     2 * q.z * q.x - 2 * q.w * q.y, 0 );
	y.Set(     2 * q.x * q.y - 2 * q.w * q.z, 1 - 2 * q.x * q.x - 2 * q.z * q.z,     2 * q.y * q.z + 2 * q.w * q.x, 0 );
	z.Set(     2 * q.z * q.x + 2 * q.w * q.y,     2 * q.y * q.z - 2 * q.w * q.x, 1 - 2 * q.x * q.x - 2 * q.y * q.y, 0 );
}

Mat4::Mat4( const Rect &r ) : z(kUnitZAxis)
{
	x.Set( r.w, 0,   0, 0 );
	y.Set( 0,   r.h, 0, 0 );
	z.Set( r.x, r.y, 0, 1 );
}

Mat4::Mat4( float f )
{
	x.Set( f, 0, 0, 0 ) ;
	y.Set( 0, f, 0, 0 ) ;
	z.Set( 0, 0, f, 0 ) ;
	w.Set( 0, 0, 0, f ) ;
}

Mat4::Mat4( float xx, float xy, float xz, float xw,
	float yx, float yy, float yz, float yw,
	float zx, float zy, float zz, float zw,
	float wx, float wy, float wz, float ww )
{
	x.Set( xx, xy, xz, xw ) ;
	y.Set( yx, yy, yz, yw ) ;
	z.Set( zx, zy, zz, zw ) ;
	w.Set( wx, wy, wz, ww ) ;
}

Mat4::Mat4( const Vec4 &x, const Vec4 &y, const Vec4 &z, const Vec4 &w )
{
	Set( x, y, z, w ) ;
}

Mat4 &Mat4::Set( float xx, float xy, float xz, float xw,
	float yx, float yy, float yz, float yw,
	float zx, float zy, float zz, float zw,
	float wx, float wy, float wz, float ww )
{
	x.Set( xx, xy, xz, xw ) ;
	y.Set( yx, yy, yz, yw ) ;
	z.Set( zx, zy, zz, zw ) ;
	w.Set( wx, wy, wz, ww ) ;
	return *this ;
}

Mat4 &Mat4::Set( const Vec4 &x, const Vec4 &y, const Vec4 &z, const Vec4 &w )
{
	this->x = x ;
	this->y = y ;
	this->z = z ;
	this->w = w ;
	return *this ;
}

Mat4 &Mat4::operator=( float f )
{
	x.Set( f, 0, 0, 0 ) ;
	y.Set( 0, f, 0, 0 ) ;
	z.Set( 0, 0, f, 0 ) ;
	w.Set( 0, 0, 0, f ) ;
	return *this ;
}

Vec4 &Mat4::operator[]( int num )
{
	return ( (Vec4 *)&x )[ num ] ;
}

const Vec4 &Mat4::operator[]( int num ) const
{
	return ( (const Vec4 *)&x )[ num ] ;
}

bool Mat4::operator==( const Mat4 &m ) const
{
	return ( x == m.x && y == m.y && z == m.z && w == m.w ) ;
}

bool Mat4::operator!=( const Mat4 &m ) const
{
	return ( x != m.x || y != m.y || z != m.z || w != m.w ) ;
}

bool Mat4::operator<( const Mat4 &m ) const
{
	if ( x != m.x ) return ( x < m.x ) ;
	if ( y != m.y ) return ( y < m.y ) ;
	if ( z != m.z ) return ( z < m.z ) ;
	return ( w < m.w ) ;
}

//----------------------------------------------------------------
//  vector 2 functions
//----------------------------------------------------------------

Vec2	operator+( const Vec2 &v1, const Vec2 &v2 )
{
	return Vec2( v1.x + v2.x, v1.y + v2.y ) ;
}

Vec2	operator-( const Vec2 &v1, const Vec2 &v2 )
{
	return Vec2( v1.x - v2.x, v1.y - v2.y ) ;
}

Vec2	operator-( const Vec2 &v1 )
{
	return Vec2( - v1.x, - v1.y ) ;
}

Vec2	operator*( const Vec2 &v1, const Vec2 &v2 )
{
	return Vec2( v1.x * v2.x, v1.y * v2.y ) ;
}

Vec2	operator*( const Vec2 &v1, float f )
{
	return Vec2( v1.x * f, v1.y * f ) ;
}

Vec2	operator*( float f, const Vec2 &v1 )
{
	return Vec2( v1.x * f, v1.y * f ) ;
}

Vec2	operator/( const Vec2 &v1, const Vec2 &v2 )
{
	float x = ( v2.x == 0.0f ) ? 0.0f : v1.x / v2.x ;
	float y = ( v2.y == 0.0f ) ? 0.0f : v1.y / v2.y ;
	return Vec2( x, y ) ;
}

Vec2	operator/( const Vec2 &v1, float f )
{
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	return Vec2( v1.x * f, v1.y * f ) ;
}

Vec2	operator/( float f, const Vec2 &v1 )
{
	float x = ( v1.x == 0.0f ) ? 0.0f : f / v1.x ;
	float y = ( v1.y == 0.0f ) ? 0.0f : f / v1.y ;

	return Vec2( x, y ) ;
}

Vec2 &	operator+=( Vec2 &v1, const Vec2 &v2 )
{
	v1.x += v2.x ;
	v1.y += v2.y ;
	return v1 ;
}

Vec2 &	operator-=( Vec2 &v1, const Vec2 &v2 )
{
	v1.x -= v2.x ;
	v1.y -= v2.y ;
	return v1 ;
}

Vec2 &	operator*=( Vec2 &v1, const Vec2 &v2 )
{
	v1.x *= v2.x ;
	v1.y *= v2.y ;
	return v1 ;
}

Vec2 &	operator*=( Vec2 &v1, float f )
{
	v1.x *= f ;
	v1.y *= f ;
	return v1 ;
}

Vec2 &	operator/=( Vec2 &v1, const Vec2 &v2 )
{
	v1.x = ( v2.x == 0.0f ) ? 0.0f : v1.x / v2.x ;
	v1.y = ( v2.y == 0.0f ) ? 0.0f : v1.y / v2.y ;
	return v1 ;
}

Vec2 &	operator/=( Vec2 &v1, float f )
{
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	v1.x *= f ;
	v1.y *= f ;
	return v1 ;
}


//----------------------------------------------------------------
//  vector 4 functions
//----------------------------------------------------------------

Vec4	operator+( const Vec4 &v1, const Vec4 &v2 )
{
	return Vec4( v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w ) ;
}

Vec4	operator-( const Vec4 &v1, const Vec4 &v2 )
{
	return Vec4( v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w ) ;
}

Vec4	operator-( const Vec4 &v1 )
{
	return Vec4( - v1.x, - v1.y, - v1.z, - v1.w ) ;
}

Vec4	operator*( const Vec4 &v1, const Vec4 &v2 )
{
	return Vec4( v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w ) ;
}

Vec4	operator*( const Vec4 &v1, float f )
{
	return Vec4( v1.x * f, v1.y * f, v1.z * f, v1.w * f ) ;
}

Vec4	operator*( float f, const Vec4 &v1 )
{
	return Vec4( v1.x * f, v1.y * f, v1.z * f, v1.w * f ) ;
}

Vec4	operator/( const Vec4 &v1, const Vec4 &v2 )
{
	float x = ( v2.x == 0.0f ) ? 0.0f : v1.x / v2.x ;
	float y = ( v2.y == 0.0f ) ? 0.0f : v1.y / v2.y ;
	float z = ( v2.z == 0.0f ) ? 0.0f : v1.z / v2.z ;
	float w = ( v2.w == 0.0f ) ? 0.0f : v1.w / v2.w ;
	return Vec4( x, y, z, w ) ;
}

Vec4	operator/( const Vec4 &v1, float f )
{
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	return Vec4( v1.x * f, v1.y * f, v1.z * f, v1.w * f ) ;
}

Vec4	operator/( float f, const Vec4 &v1 )
{
	float x = ( v1.x == 0.0f ) ? 0.0f : f / v1.x ;
	float y = ( v1.y == 0.0f ) ? 0.0f : f / v1.y ;
	float z = ( v1.z == 0.0f ) ? 0.0f : f / v1.z ;
	float w = ( v1.w == 0.0f ) ? 0.0f : f / v1.w ;
	return Vec4( x, y, z, w ) ;
}

Vec4 &	operator+=( Vec4 &v1, const Vec4 &v2 )
{
	v1.x += v2.x ;
	v1.y += v2.y ;
	v1.z += v2.z ;
	v1.w += v2.w ;
	return v1 ;
}

Vec4 &	operator-=( Vec4 &v1, const Vec4 &v2 )
{
	v1.x -= v2.x ;
	v1.y -= v2.y ;
	v1.z -= v2.z ;
	v1.w -= v2.w ;
	return v1 ;
}

Vec4 &	operator*=( Vec4 &v1, const Vec4 &v2 )
{
	v1.x *= v2.x ;
	v1.y *= v2.y ;
	v1.z *= v2.z ;
	v1.w *= v2.w ;
	return v1 ;
}

Vec4 &	operator*=( Vec4 &v1, float f )
{
	v1.x *= f ;
	v1.y *= f ;
	v1.z *= f ;
	v1.w *= f ;
	return v1 ;
}

Vec4 &	operator/=( Vec4 &v1, const Vec4 &v2 )
{
	v1.x = ( v2.x == 0.0f ) ? 0.0f : v1.x / v2.x ;
	v1.y = ( v2.y == 0.0f ) ? 0.0f : v1.y / v2.y ;
	v1.z = ( v2.z == 0.0f ) ? 0.0f : v1.z / v2.z ;
	v1.w = ( v2.w == 0.0f ) ? 0.0f : v1.w / v2.w ;
	return v1 ;
}

Vec4 &	operator/=( Vec4 &v1, float f )
{
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	v1.x *= f ;
	v1.y *= f ;
	v1.z *= f ;
	v1.w *= f ;
	return v1 ;
}



///////////////

Vec3	operator+( const Vec3 &v1, const Vec3 &v2 )
{
	return Vec3( v1.x + v2.x, v1.y + v2.y, v1.z + v2.z ) ;
}

Vec3	operator-( const Vec3 &v1, const Vec3 &v2 )
{
	return Vec3( v1.x - v2.x, v1.y - v2.y, v1.z - v2.z ) ;
}

Vec3	operator-( const Vec3 &v1 )
{
	return Vec3( - v1.x, - v1.y, - v1.z ) ;
}

Vec3	operator*( const Vec3 &v1, const Vec3 &v2 )
{
	return Vec3( v1.x * v2.x, v1.y * v2.y, v1.z * v2.z ) ;
}

Vec3	operator*( const Vec3 &v1, float f )
{
	return Vec3( v1.x * f, v1.y * f, v1.z * f ) ;
}

Vec3	operator*( float f, const Vec3 &v1 )
{
	return Vec3( v1.x * f, v1.y * f, v1.z * f ) ;
}

Vec3	operator/( const Vec3 &v1, const Vec3 &v2 )
{
	float x = ( v2.x == 0.0f ) ? 0.0f : v1.x / v2.x ;
	float y = ( v2.y == 0.0f ) ? 0.0f : v1.y / v2.y ;
	float z = ( v2.z == 0.0f ) ? 0.0f : v1.z / v2.z ;
	return Vec3( x, y, z ) ;
}

Vec3	operator/( const Vec3 &v1, float f )
{
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	return Vec3( v1.x * f, v1.y * f, v1.z * f ) ;
}

Vec3	operator/( float f, const Vec3 &v1 )
{
	float x = ( v1.x == 0.0f ) ? 0.0f : f / v1.x ;
	float y = ( v1.y == 0.0f ) ? 0.0f : f / v1.y ;
	float z = ( v1.z == 0.0f ) ? 0.0f : f / v1.z ;
	return Vec3( x, y, z) ;
}

Vec3 &	operator+=( Vec3 &v1, const Vec3 &v2 )
{
	v1.x += v2.x ;
	v1.y += v2.y ;
	v1.z += v2.z ;
	return v1 ;
}

Vec3 &	operator-=( Vec3 &v1, const Vec3 &v2 )
{
	v1.x -= v2.x ;
	v1.y -= v2.y ;
	v1.z -= v2.z ;
	return v1 ;
}

Vec3 &	operator*=( Vec3 &v1, const Vec3 &v2 )
{
	v1.x *= v2.x ;
	v1.y *= v2.y ;
	v1.z *= v2.z ;
	return v1 ;
}

Vec3 &	operator*=( Vec3 &v1, float f )
{
	v1.x *= f ;
	v1.y *= f ;
	v1.z *= f ;
	return v1 ;
}

Vec3 &	operator/=( Vec3 &v1, const Vec3 &v2 )
{
	v1.x = ( v2.x == 0.0f ) ? 0.0f : v1.x / v2.x ;
	v1.y = ( v2.y == 0.0f ) ? 0.0f : v1.y / v2.y ;
	v1.z = ( v2.z == 0.0f ) ? 0.0f : v1.z / v2.z ;
	return v1 ;
}

Vec3 &	operator/=( Vec3 &v1, float f )
{
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	v1.x *= f ;
	v1.y *= f ;
	v1.z *= f ;
	return v1 ;
}


Vec3	operator*( const Vec4 &v1, const Vec3 &v2 )
{
	return  Vec3( v1.x * v2.x, v1.y * v2.y, v1.z * v2.z ) ;
}

bool	Equals2( const Vec4 &v1, const Vec4 &v2, float e )
{
	if ( fabsf( v1.x - v2.x ) > e ) return false ;
	if ( fabsf( v1.y - v2.y ) > e ) return false ;
	return true ;
}

float	Dot2( const Vec4 &v1, const Vec4 &v2 )
{
	return v1.x * v2.x + v1.y * v2.y ;
}

float	Length2( const Vec4 &v1 )
{
	return sqrtf( Dot2( v1, v1 ) ) ;
}

Vec4	Normalize2( const Vec4 &v1 )
{
	float f = sqrtf( Dot2( v1, v1 ) ) ;
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	return Vec4( v1.x * f, v1.y * f, v1.z, v1.w ) ;
}

Vec4	Lerp2( const Vec4 &v1, const Vec4 &v2, float rate )
{
	float x = ( v2.x - v1.x ) * rate + v1.x ;
	float y = ( v2.y - v1.y ) * rate + v1.y ;
	return Vec4( x, y, v1.z, v1.w ) ;
}

Vec4	Clamp2( const Vec4 &v1, float min, float max )
{
	float x = ( v1.x < min ) ? min : ( v1.x > max ) ? max : v1.x ;
	float y = ( v1.y < min ) ? min : ( v1.y > max ) ? max : v1.y ;
	return Vec4( x, y, v1.z, v1.w ) ;
}

bool	Equals3( const Vec4 &v1, const Vec4 &v2, float e )
{
	if ( fabsf( v1.x - v2.x ) > e ) return false ;
	if ( fabsf( v1.y - v2.y ) > e ) return false ;
	if ( fabsf( v1.z - v2.z ) > e ) return false ;
	return true ;
}

float	Dot3( const Vec4 &v1, const Vec4 &v2 )
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z ;
}

Vec4	Cross3( const Vec4 &v1, const Vec4 &v2 )
{
	float x = v1.y * v2.z - v2.y * v1.z ;
	float y = v1.z * v2.x - v2.z * v1.x ;
	float z = v1.x * v2.y - v2.x * v1.y ;
	return Vec4( x, y, z, v1.w ) ;
}

float	Length3( const Vec4 &v1 )
{
	return sqrtf( Dot3( v1, v1 ) ) ;
}

Vec4	Normalize3( const Vec4 &v1 )
{
	float f = sqrtf( Dot3( v1, v1 ) ) ;
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	return Vec4( v1.x * f, v1.y * f, v1.z * f, v1.w ) ;
}

Vec4	Lerp3( const Vec4 &v1, const Vec4 &v2, float rate )
{
	float x = ( v2.x - v1.x ) * rate + v1.x ;
	float y = ( v2.y - v1.y ) * rate + v1.y ;
	float z = ( v2.z - v1.z ) * rate + v1.z ;
	return Vec4( x, y, z, v1.w ) ;
}

Vec4	Clamp3( const Vec4 &v1, float min, float max )
{
	float x = ( v1.x < min ) ? min : ( v1.x > max ) ? max : v1.x ;
	float y = ( v1.y < min ) ? min : ( v1.y > max ) ? max : v1.y ;
	float z = ( v1.z < min ) ? min : ( v1.z > max ) ? max : v1.z ;
	return Vec4( x, y, z, v1.w ) ;
}

bool	Equals4( const Vec4 &v1, const Vec4 &v2, float e )
{
	if ( fabsf( v1.x - v2.x ) > e ) return false ;
	if ( fabsf( v1.y - v2.y ) > e ) return false ;
	if ( fabsf( v1.z - v2.z ) > e ) return false ;
	if ( fabsf( v1.w - v2.w ) > e ) return false ;
	return true ;
}

float	Dot4( const Vec4 &v1, const Vec4 &v2 )
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z ;
}

float	Length4( const Vec4 &v1 )
{
	return sqrtf( Dot4( v1, v1 ) ) ;
}

Vec4	Normalize4( const Vec4 &v1 )
{
	float f = sqrtf( Dot4( v1, v1 ) ) ;
	if ( f == 0.0f ) return v1 ;
	f = 1.0f / f ;
	return Vec4( v1.x * f, v1.y * f, v1.z * f, v1.w ) ;
}

Vec4	Lerp4( const Vec4 &v1, const Vec4 &v2, float rate )
{
	float x = ( v2.x - v1.x ) * rate + v1.x ;
	float y = ( v2.y - v1.y ) * rate + v1.y ;
	float z = ( v2.z - v1.z ) * rate + v1.z ;
	float w = ( v2.w - v1.w ) * rate + v1.w ;
	return Vec4( x, y, z, w ) ;
}

Vec4	Clamp4( const Vec4 &v1, float min, float max )
{
	float x = ( v1.x < min ) ? min : ( v1.x > max ) ? max : v1.x ;
	float y = ( v1.y < min ) ? min : ( v1.y > max ) ? max : v1.y ;
	float z = ( v1.z < min ) ? min : ( v1.z > max ) ? max : v1.z ;
	float w = ( v1.w < min ) ? min : ( v1.w > max ) ? max : v1.w ;
	return Vec4( x, y, z, w ) ;
}

float	Distance( const Vec3 &v1, const Vec3 &v2 )
{
	Vec3 delta = v2 - v1;
	return sqrtf( Dot( delta, delta ) );
}

float	DistanceSqr( const Vec3 &v1, const Vec3 &v2 )
{
	Vec3 delta = v2 - v1;
	return Dot(delta, delta);
}

bool	Equals( const Vec3 &v1, const Vec3 &v2, float e )
{
	if ( fabsf( v1.x - v2.x ) > e ) return false ;
	if ( fabsf( v1.y - v2.y ) > e ) return false ;
	if ( fabsf( v1.z - v2.z ) > e ) return false ;
	return true ;
}

float	Dot( const Vec3 &v1, const Vec3 &v2 )
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z ;
}

Vec3	Cross( const Vec3 &v1, const Vec3 &v2 )
{
	float x = v1.y * v2.z - v2.y * v1.z ;
	float y = v1.z * v2.x - v2.z * v1.x ;
	float z = v1.x * v2.y - v2.x * v1.y ;
	return Vec3( x, y, z ) ;
}

float	Length( const Vec3 &v1 )
{
	return sqrtf( Dot( v1, v1 ) ) ;
}

Vec3	Normalize( const Vec3 &v1 )
{
	float f = sqrtf( Dot( v1, v1 ) ) ;
	if ( f == 0.0f )
		return v1 ;
	f = 1.0f / f ;
	return Vec3( v1.x * f, v1.y * f, v1.z * f ) ;
}

Vec3	Lerp( const Vec3 &v1, const Vec3 &v2, float rate )
{
	float x = ( v2.x - v1.x ) * rate + v1.x ;
	float y = ( v2.y - v1.y ) * rate + v1.y ;
	float z = ( v2.z - v1.z ) * rate + v1.z ;
	return Vec4( x, y, z ) ;
}

Vec3	Clamp( const Vec3 &v1, float min, float max )
{
	float x = ( v1.x < min ) ? min : ( v1.x > max ) ? max : v1.x ;
	float y = ( v1.y < min ) ? min : ( v1.y > max ) ? max : v1.y ;
	float z = ( v1.z < min ) ? min : ( v1.z > max ) ? max : v1.z ;
	return Vec3( x, y, z ) ;
}

bool	IsBetweenPoints( const Vec3 &point, const Vec3 &begin, const Vec3 &end )
{
	float f = DistanceSqr(begin, end);
	return (DistanceSqr(point, begin) < f) && (DistanceSqr(point, end) < f);

}

float	Dot( const Vec2 &v1, const Vec2 &v2 )
{
	return v1.x * v2.x + v1.y * v2.y;
}


//----------------------------------------------------------------
//  matrix functions
//----------------------------------------------------------------

Mat4	operator+( const Mat4 &m1, const Mat4 &m2 )
{
	return Mat4( m1.x + m2.x, m1.y + m2.y, m1.z + m2.z, m1.w + m2.w ) ;
}

Mat4	operator-( const Mat4 &m1, const Mat4 &m2 )
{
	return Mat4( m1.x - m2.x, m1.y - m2.y, m1.z - m2.z, m1.w - m2.w ) ;
}

Mat4	operator-( const Mat4 &m1 )
{
	return Mat4( - m1.x, - m1.y, - m1.z, - m1.w ) ;
}

Mat4	operator*( const Mat4 &m1, const Mat4 &m2 )
{
	Vec4 x = Transform4( m1, m2.x ) ;
	Vec4 y = Transform4( m1, m2.y ) ;
	Vec4 z = Transform4( m1, m2.z ) ;
	Vec4 w = Transform4( m1, m2.w ) ;
	return Mat4( x, y, z, w ) ;
}

Mat4	operator*( const Mat4 &m1, float f )
{
	return Mat4( m1.x * f, m1.y * f, m1.z * f, m1.w * f ) ;
}

Mat4	operator*( float f, const Mat4 &m1 )
{
	return Mat4( m1.x * f, m1.y * f, m1.z * f, m1.w * f ) ;
}

Vec4	operator*( const Mat4 &m1, const Vec4 &v1 )
{
	return Transform3( m1, v1 ) ;
}
Vec3	operator*( const Mat4 &m1, const Vec3 &v1 )
{
	return Transform( m1, v1 ) ;
}
Mat4	operator/( const Mat4 &m1, float f )
{
	if ( f == 0.0f ) return m1 ;
	f = 1.0f / f ;
	return Mat4( m1.x * f, m1.y * f, m1.z * f, m1.w * f ) ;
}

Mat4 &	operator+=( Mat4 &m1, const Mat4 &m2 )
{
	m1.x += m2.x ;
	m1.y += m2.y ;
	m1.z += m2.z ;
	m1.w += m2.w ;
	return m1 ;
}

Mat4 &	operator-=( Mat4 &m1, const Mat4 &m2 )
{
	m1.x -= m2.x ;
	m1.y -= m2.y ;
	m1.z -= m2.z ;
	m1.w -= m2.w ;
	return m1 ;
}

Mat4 &	operator*=( Mat4 &m1, const Mat4 &m2 )
{
	Mat4 m = m1 ;
	m1.x = Transform4( m, m2.x ) ;
	m1.y = Transform4( m, m2.y ) ;
	m1.z = Transform4( m, m2.z ) ;
	m1.w = Transform4( m, m2.w ) ;
	return m1 ;
}

Mat4 &	operator*=( Mat4 &m1, float f )
{
	m1.x *= f ;
	m1.y *= f ;
	m1.z *= f ;
	m1.w *= f ;
	return m1 ;
}

Mat4 &	operator/=( Mat4 &m1, float f )
{
	if ( f == 0.0f ) return m1 ;
	f = 1.0f / f ;
	m1.x *= f ;
	m1.y *= f ;
	m1.z *= f ;
	m1.w *= f ;
	return m1 ;
}

bool	Equals( const Mat4 &m1, const Mat4 &m2, float e )
{
	return Equals( m1, m2, e, e ) ;
}

bool	Equals( const Mat4 &m1, const Mat4 &m2, float e, float ew )
{
	if ( !Equals4( m1.x, m2.x, e ) ) return false ;
	if ( !Equals4( m1.y, m2.y, e ) ) return false ;
	if ( !Equals4( m1.z, m2.z, e ) ) return false ;
	if ( !Equals4( m1.w, m2.w, ew ) ) return false ;
	return true ;
}

Mat4	Transpose( const Mat4 &m1 )
{
	return Mat4( m1.x.x, m1.y.x, m1.z.x, m1.w.x,
		m1.x.y, m1.y.y, m1.z.y, m1.w.y,
		m1.x.z, m1.y.z, m1.z.z, m1.w.z,
		m1.x.w, m1.y.w, m1.z.w, m1.w.w ) ;
}

Mat4	Normalize( const Mat4 &m1 )
{
	return Mat4( Normalize3( m1.x ),
		Normalize3( m1.y ),
		Normalize3( m1.z ),
		m1.w ) ;
}

Mat4	Inverse( const Mat4 &m1 )
{
	float source[ 4 ][ 4 ], inverse[ 4 ][ 4 ] ;
	int i, j, k ;
	float tmp, d ;

	for ( i = 0 ; i < 4 ; i ++ ) {
		for ( j = 0 ; j < 4 ; j ++ ) {
			source[ i ][ j ] = ( m1[ i ][ j ] ) ;
			inverse[ i ][ j ] = ( i == j ) ? 1.0f : 0.0f ;
		}
	}

	for ( i = 0 ; i < 4 ; i ++ ) {
		d = 0.0f ;
		j = -1 ;
		for ( k = i ; k < 4 ; k ++ ) {
			tmp = source[ k ][ i ] ;
			if ( fabsf( tmp ) > fabsf( d ) ) {
				d = tmp ;
				j = k ;
			}
		}
		if ( j < 0 )
			return Mat4( kIdentity );

		if ( j != i ) {
			for ( k = 0 ; k < 4 ; k ++ ) {
				tmp = source[ i ][ k ] ;
				source[ i ][ k ] = source[ j ][ k ] ;
				source[ j ][ k ] = tmp ;
				tmp = inverse[ i ][ k ] ;
				inverse[ i ][ k ] = inverse[ j ][ k ] ;
				inverse[ j ][ k ] = tmp ;
			}
		}
		for ( j = 0 ; j < 4 ; j ++ ) {
			if ( j == i ) source[ i ][ j ] = 1.0f ;
			if ( j > i ) source[ i ][ j ] /= d ;
			inverse[ i ][ j ] /= d ;
		}
		for ( j = 0 ; j < 4 ; j ++ ) {
			if ( j == i ) continue ;
			d = source[ j ][ i ] ;
			for ( k = 0 ; k < 4 ; k ++ ) {
				if ( k == i ) source[ j ][ k ] = 0.0f ;
				if ( k > i ) source[ j ][ k ] -= source[ i ][ k ] * d ;
				inverse[ j ][ k ] -= inverse[ i ][ k ] * d ;
			}
		}
	}
	Mat4 m ;
	for ( i = 0 ; i < 4 ; i ++ ) {
		for ( j = 0 ; j < 4 ; j ++ ) {
			m[ i ][ j ] = (float)( inverse[ i ][ j ] ) ;
		}
	}
	return m ;
}

Mat4	InverseOrtho( const Mat4 &m1 )
{
	float sx = 1.0f / Dot3( m1.x, m1.x ) ;
	float sy = 1.0f / Dot3( m1.y, m1.y ) ;
	float sz = 1.0f / Dot3( m1.z, m1.z ) ;

	float x = - m1.w.x ;
	float y = - m1.w.y ;
	float z = - m1.w.z ;
	return Mat4( m1.x.x * sx, m1.y.x * sy, m1.z.x * sz, 0,
		m1.x.y * sx, m1.y.y * sy, m1.z.y * sz, 0,
		m1.x.z * sx, m1.y.z * sy, m1.z.z * sz, 0,
		( m1.x.x * x + m1.x.y * y + m1.x.z * z ) * sx,
		( m1.y.x * x + m1.y.y * y + m1.y.z * z ) * sy,
		( m1.z.x * x + m1.z.y * y + m1.z.z * z ) * sz, 1 ) ;
}

Mat4	InverseOrthonormal( const Mat4 &m1 )
{
	float x = - m1.w.x ;
	float y = - m1.w.y ;
	float z = - m1.w.z ;
	return Mat4( m1.x.x, m1.y.x, m1.z.x, 0,
		m1.x.y, m1.y.y, m1.z.y, 0,
		m1.x.z, m1.y.z, m1.z.z, 0,
		m1.x.x * x + m1.x.y * y + m1.x.z * z,
		m1.y.x * x + m1.y.y * y + m1.y.z * z,
		m1.z.x * x + m1.z.y * y + m1.z.z * z, 1 ) ;
}

Mat4	Lerp( const Mat4 &m1, const Mat4 &m2, float rate )
{
	return Mat4( Lerp4( m1.x, m2.x, rate ),
		Lerp4( m1.y, m2.y, rate ),
		Lerp4( m1.z, m2.z, rate ),
		Lerp4( m1.w, m2.w, rate ) ) ;
}

Vec4	Transform2( const Mat4 &m1, const Vec4 &v1, float z, float w )
{
	return m1.x * v1.x + m1.y * v1.y + m1.z * z + m1.w * w ;
}

Vec4	Project2( const Mat4 &m1, const Vec4 &v1, float z, float w )
{
	Vec4 v2 = Transform2( m1, v1, z, w ) ;
	float w2 = v2.w ;
	if ( w2 == 0 ) return v2 ;
	w2 = 1.0f / w2 ;
	return Vec4( v2.x * w2, v2.y * w2, v2.z * w2, w2 ) ;
}

Vec4	Transform3( const Mat4 &m1, const Vec4 &v1, float w )
{
	return m1.x * v1.x + m1.y * v1.y + m1.z * v1.z + m1.w * w ;
}

Vec4	Project3( const Mat4 &m1, const Vec4 &v1, float w )
{
	Vec4 v2 = Transform3( m1, v1, w ) ;
	float w2 = v2.w ;
	if ( w2 == 0 ) return v2 ;
	w2 = 1.0f / w2 ;
	return Vec4( v2.x * w2, v2.y * w2, v2.z * w2, w2 ) ;
}

Vec4	Transform4( const Mat4 &m1, const Vec4 &v1 )
{
	return m1.x * v1.x + m1.y * v1.y + m1.z * v1.z + m1.w * v1.w ;
}

Vec4	Project4( const Mat4 &m1, const Vec4 &v1 )
{
	Vec4 v2 = Transform4( m1, v1 ) ;
	float w2 = v2.w ;
	if ( w2 == 0 ) return v2 ;
	w2 = 1.0f / w2 ;
	return Vec4( v2.x * w2, v2.y * w2, v2.z * w2, w2 ) ;
}

Vec3	Transform( const Mat4 &m1, const Vec3 &v1 )
{
	return m1.x * v1.x + m1.y * v1.y + m1.z * v1.z + m1.w;
}


namespace Matrix
{
	Mat4 RotateTranslate( const Quat &quat, const Vec4 &t)
	{
		return RotateTranslate(quat, t.x, t.y, t.z );
	}

	Mat4 RotateTranslate( const Quat &quat, float x, float y, float z)
	{
		Mat4 m(quat);
		m[3] = Vec4(x, y, z, 1.0f);
		return m;
	}


	Mat4 Translate( const Vec4 &v1 )
	{
		return Translate( v1.x, v1.y, v1.z ) ;
	}

	Mat4 Translate( float x, float y, float z )
	{
		return Mat4( 1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			x, y, z, 1 ) ;
	}

	Mat4 Rotate( const Vec4 &axis, float angle )
	{
		return Mat4( Quaternion::Rotate( axis, angle ) ) ;
		//Vec4 v = Normalize3(axis);
		//float c = cosf(angle);
		//float s = sinf(angle);
		//return Mat4(
		//  1 + (1-c)*(v.x*v.x-1),		-v.z*s+(1-c)*v.x*v.y,		v.y*s+(1-c)*v.x*v.z,		0,
		//  v.z*s+(1-c)*v.x*v.y,		1 + (1-c)*(v.y*v.y-1),		-v.x*s+(1-c)*v.y*v.z,		0,
		//  -v.y*s+(1-c)*v.x*v.z,	v.x*s+(1-c)*v.y*v.z,		1 + (1-c)*(v.z*v.z-1),		0,
		//  0, 0, 0, 1 );
	}

	Mat4 Rotate( float x, float y, float z, float angle )
	{
		//return Rotate( Vec4(x, y, z), angle );
		return Mat4( Quaternion::Rotate( x, y, z, angle ) ) ;
	}

	Mat4 Rotx( float x )
	{
		float c = cosf( x ) ;
		float s = sinf( x ) ;
		return Mat4(
			1, 0, 0, 0,
			0, c,-s, 0,
			0, s, c, 0,
			0, 0, 0, 1 ) ;
	}

	Mat4 Roty( float y )
	{
		float c = cosf( y ) ;
		float s = sinf( y ) ;
		return Mat4(
			c, 0, s, 0,
			0, 1, 0, 0,
		   -s, 0, c, 0,
			0, 0, 0, 1 ) ;
	}

	Mat4 Rotz( float z )
	{
		float c = cosf( z ) ;
		float s = sinf( z ) ;
		return Mat4(
			c,-s, 0, 0,
			s, c, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1 ) ;
	}

	Mat4 Rotzyx( const Vec4 &rotate )
	{
		return Rotzyx( rotate.x, rotate.y, rotate.z ) ;
	}

	Mat4 Rotyxz( const Vec4 &rotate )
	{
		return Rotyxz( rotate.x, rotate.y, rotate.z ) ;
	}

	Mat4 Rotxzy( const Vec4 &rotate )
	{
		return Rotxzy( rotate.x, rotate.y, rotate.z ) ;
	}

	Mat4 Rotxyz( const Vec4 &rotate )
	{
		return Rotxyz( rotate.x, rotate.y, rotate.z ) ;
	}

	Mat4 Rotyzx( const Vec4 &rotate )
	{
		return Rotyzx( rotate.x, rotate.y, rotate.z ) ;
	}

	Mat4 Rotzxy( const Vec4 &rotate )
	{
		return Rotzxy( rotate.x, rotate.y, rotate.z ) ;
	}

	Mat4 Rotzyx( float x, float y, float z )
	{
		return Mat4( Quaternion::Rotzyx( x, y, z ) ) ;
	}

	Mat4 Rotyxz( float x, float y, float z )
	{
		return Mat4( Quaternion::Rotyxz( x, y, z ) ) ;
	}

	Mat4 Rotxzy( float x, float y, float z )
	{
		return Mat4( Quaternion::Rotxzy( x, y, z ) ) ;
	}

	Mat4 Rotxyz( float x, float y, float z )
	{
		return Mat4( Quaternion::Rotxyz( x, y, z ) ) ;
	}

	Mat4 Rotyzx( float x, float y, float z )
	{
		return Mat4( Quaternion::Rotyzx( x, y, z ) ) ;
	}

	Mat4 Rotzxy( float x, float y, float z )
	{
		return Mat4( Quaternion::Rotzxy( x, y, z ) ) ;
	}

	Mat4 Scale( const Vec4 &scale )
	{
		return Scale( scale.x, scale.y, scale.z ) ;
	}

	Mat4 Scale( float x, float y, float z )
	{
		return Mat4( x, 0, 0, 0,
			0, y, 0, 0,
			0, 0, z, 0,
			0, 0, 0, 1 ) ;
	}

	Mat4 Viewport( float x, float y, float w, float h, float z_near, float z_far )
	{
		w *= 0.5f ;  x += w ;
		h *= -0.5f ; y -= h ;
		float d = ( z_far - z_near ) * 0.5f ;
		float z = ( z_far + z_near ) * 0.5f ;
		return Mat4( w, 0, 0, 0,
			0, h, 0, 0,
			0, 0, d, 0,
			x, y, z, 1 ) ;
	}

	Mat4 Perspective( float fovy, float aspect, float z_near, float z_far )
	{
		float f = 1.0f / tanf( fovy * 0.5f ) ;
		float e = ( z_near - z_far ) ;
		float a = ( z_far + z_near ) / e ;
		float b = ( 2.0f * z_far * z_near ) / e ;
		Mat4 mat( f / aspect, 0, 0, 0,
			0, f, 0, 0,
			0, 0, a, -1,
			0, 0, b, 0 ) ;
		return mat;
	}

	Mat4 Frustum( float left, float right, float bottom, float top, float z_near, float z_far )
	{
		float w = 1.0f / ( right - left ) ;
		float h = 1.0f / ( top - bottom ) ;
		float d = 1.0f / ( z_far - z_near ) ;
		float A = ( right + left ) * w ;
		float B = ( top + bottom ) * h ;
		float C = - ( z_far + z_near ) * d ;
		float D = - 2 * z_far * z_near * d ;
		return Mat4( 2 * z_near * w, 0, 0, 0,
			0, 2 * z_near * h, 0, 0,
			A, B, C, -1,
			0, 0, D, 0 ) ;
	}

	Mat4 Ortho( float left, float right, float bottom, float top, float z_near, float z_far )
	{
		float w = 1.0f / ( right - left ) ;
		float h = 1.0f / ( top - bottom ) ;
		float d = 1.0f / ( z_far - z_near ) ;
		float A = - ( right + left ) * w ;
		float B = - ( top + bottom ) * h ;
		float C = - ( z_far + z_near ) * d ;
		return Mat4( 2 * w, 0, 0, 0,
			0, 2 * h, 0, 0,
			0, 0, -2 * d, 0,
			A, B, C, 1 ) ;
	}

	Mat4 Lookat( const Vec4 &eye, const Vec4 &center, const Vec4 &up )
	{
		Vec4 f = Normalize3( center - eye ) ;
		Vec4 u = Normalize3( up ) ;
		Vec4 s = Normalize3( Cross3( f, u ) ) ;
		u = Cross3( s, f ) ;
		return Mat4( s.x, u.x, -f.x, 0,
			s.y, u.y, -f.y, 0,
			s.z, u.z, -f.z, 0,
			- Dot3( eye, s ), - Dot3( eye, u ), Dot3( eye, f ), 1 ) ;
	}

	Mat4 Lookat( float ex, float ey, float ez, float cx, float cy, float cz, float ux, float uy, float uz )
	{
		return Lookat( Vec4( ex, ey, ez ), Vec4( cx, cy, cz ), Vec4( ux, uy, uz ) ) ;
	}

	Mat4 Lookat( const Vec4 &dir )
	{
		Vec3 x_dir(kUnitZAxis);
		Vec3 y_dir;
		float d = dir.z;

		// avoid normalisation problems
		if (d > -0.99999999f && d<0.99999999f)
		{
			x_dir -= dir * d ;
			x_dir = Normalize3( x_dir );
			y_dir = Cross3(dir, x_dir );
		}
		else
		{
			x_dir = Vec3( dir.z, 0, -dir.x );
			y_dir = Vec3( kUnitYAxis );
		}

		// x_dir and y_dir is orthogonal to dir and to eachother.
		// so, you can make matrix from x_dir, y_dir, and dir in whatever way you prefer. 
		// What to do depends to what API you use and where arrow model is pointing.
		// this is matrix i use which may give starting point. 
		// this is for arrow that points in z direction (for arrow that points in x direction you may try swapping dir and x_dir)

		return Mat4(	x_dir.x,	x_dir.y,	x_dir.z,	0,
			y_dir.x,	y_dir.y,	y_dir.z,	0,
			dir.x,		dir.y,		dir.z,		0,
			0,			0,			0,			1.0f );
	}

} // end namespace MakeMat4

//----------------------------------------------------------------
//  quaternion functions
//----------------------------------------------------------------

Quat	operator*( const Quat &q1, const Quat &q2 )
{
	float w1, x1, y1, z1 ;
	float w2, x2, y2, z2 ;

	x1 = q1.x ;	y1 = q1.y ;	z1 = q1.z ;	w1 = q1.w ;
	x2 = q2.x ;	y2 = q2.y ;	z2 = q2.z ;	w2 = q2.w ;

	return Quat( w1 * x2 + w2 * x1 + y1 * z2 - z1 * y2,
		w1 * y2 + w2 * y1 + z1 * x2 - x1 * z2,
		w1 * z2 + w2 * z1 + x1 * y2 - y1 * x2,
		w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2 ) ;
}

Vec4	operator*( const Quat &q1, const Vec4 &v1 )
{
	return Transform3( q1, v1 ) ;
}

Quat &	operator*=( Quat &q1, const Quat &q2 )
{
	return ( q1 = q1 * q2 ) ;
}

bool	Equals( const Quat &q1, const Quat &q2, float e )
{
	return ( Dot( q1, q2 ) >= cosf( e * 0.5f ) ) ;
}

float	Dot( const Quat &q1, const Quat &q2 )
{
	return q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w ;
}

float	Length( const Quat &q1 )
{
	return sqrtf( Dot( q1, q1 ) ) ;
}

Quat	Normalize( const Quat &q1 )
{
	float f = sqrtf( Dot( q1, q1 ) ) ;
	if ( f == 0.0f ) return q1 ;
	f = 1.0f / f ;
	return Quat( q1.x * f, q1.y * f, q1.z * f, q1.w * f ) ;
}

Quat	Inverse( const Quat &q1 )
{
	return Quat( -q1.x, -q1.y, -q1.z, q1.w ) ;
}

Quat	Slerp( const Quat &q1, const Quat &q2, float rate )
{
	if (rate >= 1.0f)
		return q2;
	else if (rate <= 0.0f)
		return q1;

	float c = q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w ;

	float r1 = 1 - rate ;
	float r2 = rate ;
	if ( c < 0.999999f )
	{
		if ( c < 0.0f )
		{
			c = -c ;
			r2 = -r2 ;
		}
		float angle = acosf( c ) ;
		float s = sinf( angle ) ;
		if ( s != 0.0f )
		{
			r1 = sinf( r1 * angle ) / s ;
			r2 = sinf( r2 * angle ) / s ;
		}
	}
	return Quat( q1.x * r1 + q2.x * r2,
		q1.y * r1 + q2.y * r2,
		q1.z * r1 + q2.z * r2,
		q1.w * r1 + q2.w * r2 ) ;
}

Vec4	Transform2( const Quat &q1, const Vec4 &v1, float z, float w )
{
	return Transform2( Mat4( q1 ), v1, z, w ) ;
}

Vec4	Transform3( const Quat &q1, const Vec4 &v1, float w )
{
	return Transform3( Mat4( q1 ), v1, w ) ;
}

Vec4	Transform4( const Quat &q1, const Vec4 &v1 )
{
	return Transform4( Mat4( q1 ), v1 ) ;
}

namespace Quaternion
{

	Quat Rotate( const Vec4 &axis, float angle )
	{
		angle *= 0.5f ;
		Vec4 v = Normalize3( axis ) * sinf( angle ) ;
		return Quat( v.x, v.y, v.z, cosf( angle ) ) ;
	}

	Quat Rotate( float x, float y, float z, float angle )
	{
		return Rotate( Vec4( x, y, z ), angle ) ;
	}

	Quat Rotzyx( const Vec4 &angle )
	{
		return Rotzyx( angle.x, angle.y, angle.z ) ;
	}

	Quat Rotyxz( const Vec4 &angle )
	{
		return Rotyxz( angle.x, angle.y, angle.z ) ;
	}

	Quat Rotxzy( const Vec4 &angle )
	{
		return Rotxzy( angle.x, angle.y, angle.z ) ;
	}

	Quat Rotxyz( const Vec4 &angle )
	{
		return Rotxyz( angle.x, angle.y, angle.z ) ;
	}

	Quat Rotyzx( const Vec4 &angle )
	{
		return Rotyzx( angle.x, angle.y, angle.z ) ;
	}

	Quat Rotzxy( const Vec4 &angle )
	{
		return Rotzxy( angle.x, angle.y, angle.z ) ;
	}

	Quat Rotzyx( float x, float y, float z )
	{
		x *= 0.5f ;	y *= 0.5f ;	z *= 0.5f ;
		float cx = cosf( x ) ;	float sx = sinf( x ) ;
		float cy = cosf( y ) ;	float sy = sinf( y ) ;
		float cz = cosf( z ) ;	float sz = sinf( z ) ;
		return Quat( cz * cy * sx - cx * sz * sy,
			cx * cz * sy + cy * sz * sx,
			cx * cy * sz - cz * sy * sx,
			cz * cy * cx + sz * sy * sx ) ;
	}

	Quat Rotyxz( float x, float y, float z )
	{
		x *= 0.5f ;	y *= 0.5f ;	z *= 0.5f ;
		float cx = cosf( x ) ;	float sx = sinf( x ) ;
		float cy = cosf( y ) ;	float sy = sinf( y ) ;
		float cz = cosf( z ) ;	float sz = sinf( z ) ;
		
		return Quat(
			sx * sy * sz + cx * cy * cz,
			sx * sz * cy + sy * cx * cz,
			sx * cy * cz - sy * sz * cx,
			-sx * sy * cz + sz * cx * cy
		);
		//return Quat( cz * cy * sx + cx * sy * sz,
		//	cz * cx * sy - cy * sx * sz,
		//	cy * cx * sz - cz * sy * sx,
		//	cy * cx * cz + sy * sx * sz ) ;
	}

	Quat Rotxzy( float x, float y, float z )
	{
		x *= 0.5f ;	y *= 0.5f ;	z *= 0.5f ;
		float cx = cosf( x ) ;	float sx = sinf( x ) ;
		float cy = cosf( y ) ;	float sy = sinf( y ) ;
		float cz = cosf( z ) ;	float sz = sinf( z ) ;
		return Quat( cy * cz * sx - cx * sz * sy,
			cx * cz * sy - cy * sx * sz,
			cy * cx * sz + cz * sx * sy,
			cx * cz * cy + sx * sz * sy ) ;
	}

	Quat Rotxyz( float x, float y, float z )
	{
		x *= 0.5f ;	y *= 0.5f ;	z *= 0.5f ;
		float cx = cosf( x ) ;	float sx = sinf( x ) ;
		float cy = cosf( y ) ;	float sy = sinf( y ) ;
		float cz = cosf( z ) ;	float sz = sinf( z ) ;
		return Quat( cz * cy * sx + cx * sy * sz,
			cz * cx * sy - cy * sx * sz,
			cx * cy * sz + cz * sx * sy,
			cx * cy * cz - sx * sy * sz ) ;
	}

	Quat Rotyzx( float x, float y, float z )
	{
		x *= 0.5f ;	y *= 0.5f ;	z *= 0.5f ;
		float cx = cosf( x ) ;	float sx = sinf( x ) ;
		float cy = cosf( y ) ;	float sy = sinf( y ) ;
		float cz = cosf( z ) ;	float sz = sinf( z ) ;
		return Quat( cy * cz * sx + cx * sy * sz,
			cx * cz * sy + cy * sz * sx,
			cx * cy * sz - cz * sy * sx,
			cy * cz * cx - sy * sz * sx ) ;
	}

	Quat Rotzxy( float x, float y, float z )
	{
		x *= 0.5f ;	y *= 0.5f ;	z *= 0.5f ;
		float cx = cosf( x ) ;	float sx = sinf( x ) ;
		float cy = cosf( y ) ;	float sy = sinf( y ) ;
		float cz = cosf( z ) ;	float sz = sinf( z ) ;
		return Quat( cy * cz * sx - cx * sz * sy,
			cz * cx * sy + cy * sz * sx,
			cy * cx * sz + cz * sx * sy,
			cz * cx * cy - sz * sx * sy ) ;
	}

} // end namespace Quaternion


namespace Vector4
{

	Vec4 Rotzyx( const Quat &q1 )
	{
		float x, y, z, w ;
		float cx, sx, cy, sy, cz, sz ;
		float rx, ry, rz ;

		x = q1.x ;	y = q1.y ;
		z = q1.z ;	w = q1.w ;

		sy = 2 * w * y - 2 * z * x ;
		if ( sy <= 0.99995f && sy >= -0.99995f ) {
			cz = 1 - 2 * y * y - 2 * z * z ;
			sz = 2 * x * y + 2 * w * z ;
			cx = 1 - 2 * x * x - 2 * y * y ;
			sx = 2 * y * z + 2 * w * x ;
			ry = asinf( sy ) ;
		} else {
			cz = 1 - 2 * x * x - 2 * z * z ;
			sz = 2 * w * z - 2 * x * y ;
			cx = 1 ;
			sx = 0 ;
			cy = 1 - 2 * x * x - 2 * y * y ;
			ry = atan2f( sy, cy ) ;
		}
		rx = atan2f( sx, cx ) ;
		rz = atan2f( sz, cz ) ;
		return Vec4( rx, ry, rz ) ;
	}

	Vec4 Rotyxz( const Quat &q1 )
	{
		float x, y, z, w ;
		float cx, sx, cy, sy, cz, sz ;
		float rx, ry, rz ;

		x = q1.x ;	y = q1.y ;
		z = q1.z ;	w = q1.w ;

		sx = 2 * w * x - 2 * y * z ;
		if ( sx <= 0.99995f && sx >= -0.99995f ) {
			cz = 1 - 2 * x * x - 2 * z * z ;
			sz = 2 * x * y + 2 * w * z ;
			cy = 1 - 2 * x * x - 2 * y * y ;
			sy = 2 * z * x + 2 * w * y ;
			rx = asinf( sx ) ;
		} else {
			cy = 1 - 2 * y * y - 2 * z * z ;
			sy = 2 * w * y - 2 * z * x ;
			cz = 1 ;
			sz = 0 ;
			cx = 1 - 2 * x * x - 2 * z * z ;
			rx = atan2f( sx, cx ) ;
		}
		ry = atan2f( sy, cy ) ;
		rz = atan2f( sz, cz ) ;
		return Vec4( rx, ry, rz ) ;
	}

	Vec4 Rotxyz( const Quat &q1 )
	{
		(void) q1;
		return 0.0f ;	// (>_<)
	}

} // end namespace MakeVec4

//----------------------------------------------------------------
//  rectangle functions
//----------------------------------------------------------------

Rect	operator*( const Rect &r1, const Rect &r2 )
{
	return Rect( r1.w * r2.x + r1.x, r1.h * r2.y + r1.y, r1.w * r2.w, r1.h * r2.h ) ;
}




Vec4	operator*( const Rect &r1, const Vec4 &v1 )
{
	return Transform3( r1, v1 ) ;
}

Rect &	operator*=( Rect &r1, const Rect &r2 )
{
	return ( r1 = r1 * r2 ) ;
}

bool	Equals( const Rect &r1, const Rect &r2, float e )
{
	if ( fabsf( r1.x - r2.x ) > e ) return false ;
	if ( fabsf( r1.y - r2.y ) > e ) return false ;
	if ( fabsf( r1.w - r2.w ) > e ) return false ;
	if ( fabsf( r1.h - r2.h ) > e ) return false ;
	return true ;
}

Rect	Inverse( const Rect &r1 )
{
	if ( r1.w == 0.0f || r1.h == 0.0f ) return 1.0f ;
	float w = 1.0f / r1.w ;
	float h = 1.0f / r1.h ;
	return Rect( - r1.x * w, - r1.y * h, w, h ) ;
}

Rect	Lerp( const Rect &r1, const Rect &r2, float rate )
{
	float x = ( r2.x - r1.x ) * rate + r1.x ;
	float y = ( r2.y - r1.y ) * rate + r1.y ;
	float w = ( r2.w - r1.w ) * rate + r1.w ;
	float h = ( r2.h - r1.h ) * rate + r1.h ;
	return Rect( x, y, w, h ) ;
}

Vec4	Transform2( const Rect &r1, const Vec4 &v1, float z, float w )
{
	return Vec4( v1.x * r1.w + w * r1.x, v1.y * r1.h + w * r1.y, z, w ) ;
}

Vec4	Transform3( const Rect &r1, const Vec4 &v1, float w )
{
	return Vec4( v1.x * r1.w + w * r1.x, v1.y * r1.h + w * r1.y, v1.z, w ) ;
}

Vec4	Transform4( const Rect &r1, const Vec4 &v1 )
{
	return Vec4( v1.x * r1.w + v1.w * r1.x, v1.y * r1.h + v1.w * r1.y, v1.z, v1.w ) ;
}

//----------------------------------------------------------------
//  float functions
//----------------------------------------------------------------

bool	Equals( float f1, float f2, float e )
{
	return ( fabsf( f1 - f2 ) <= e ) ;
}

bool	Equals( const float *f1, const float *f2, int count, float e )
{
	while ( -- count >= 0 ) {
		if ( fabsf( *( f1 ++ ) - *( f2 ++ ) ) > e ) return false ;
	}
	return true ;
}

}