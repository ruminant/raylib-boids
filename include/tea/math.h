#pragma once

//
// ALIGNMENT
//
#if _MSC_VER >= 1300
#define TEA_ALIGNED __declspec(align(16))
#else /* _MSC_VER >= 1300 */
#define TEA_ALIGNED
#endif /* _MSC_VER >= 1300 */

namespace tea
{

//! 8 bit unsigned variable.
typedef unsigned char		u8;

//! 8 bit signed variable.
typedef signed char			s8;

//! 8 bit character variable.
typedef char				c8;

//! 16 bit unsigned variable.
typedef unsigned short		u16;

//! 16 bit signed variable.
typedef signed short		s16;

//! 32 bit unsigned variable.
typedef unsigned int		u32;

//! 32 bit signed variable.
typedef signed int			s32;

//! 64 bit signed variable
typedef	long long			s64;

//! 64 bit unsigned variable
typedef	long long			u64;

//! 32 bit floating point variable.
typedef float				f32;

//! 64 bit floating point variable.
typedef double				f64;

//! 8-bit char is used for displaying fonts
typedef char				fnt_char;

struct Vec2 ;
struct Vec3 ;
struct Vec4 ;
struct Mat4 ;
struct Quat ;
struct Rect ;

namespace Math
{
	const 	float	kEpsilon			=	1e-6f;							// small threshold value when avoiding problems with floating point inaccuracy
	//const 	float	kMaxFloat			=	3.40282347e+38f;				// maximum float value
	const 	float	kPi					=	3.14159265358979323846f;		// pi
	const 	float	kTwoPi				=	6.28318530717958647692f;		// pi*2
	const 	float	kHalfPi				=	1.57079632679489661923f;		// pi/2
	const	float	kDegToRad			=	0.017453292519943295769236907684886f;
	const	float	kRadToDeg			=	57.295779513082320876798154814105f;
	const 	float	kLengthSqrTolerance	=	1.0e-30f;
	const	float	kRoundingError		=	0.0001f;
}

static	const	enum	ZeroTag				{	valZeroTag		}	kZero		= valZeroTag;
static	const	enum	OneTag				{	valOneTag		}	kOne		= valOneTag;
static	const	enum	IdentityTag			{	valIdentityTag	}	kIdentity	= valIdentityTag;
static	const	enum	UnitXAxisTag		{	valUnitXAxisTag	}	kUnitXAxis	= valUnitXAxisTag;
static	const	enum	UnitYAxisTag		{	valUnitYAxisTag	}	kUnitYAxis	= valUnitYAxisTag;
static	const	enum	UnitZAxisTag		{	valUnitZAxisTag	}	kUnitZAxis	= valUnitZAxisTag;
static	const	enum	UnitWAxisTag		{	valUnitWAxisTag	}	kUnitWAxis	= valUnitWAxisTag;
static	const	enum	QuatITag			{	valQuatITag		}	kQuatI		= valQuatITag;
static	const	enum	QuatJTag			{	valQuatJTag		}	kQuatJ		= valQuatJTag;
static	const	enum	QuatKTag			{	valQuatKTag		}	kQuatK		= valQuatKTag;

enum IntersectionRelation
{
	kRelationFront,
	kRelationBack,
	kRelationPlanar,
	kRelationSpanning,
	kRelationClipped
};

inline f32 Radians(f32 rad)
{
	return rad * Math::kDegToRad;
}

inline f32 Degrees(f32 rad)
{
	return rad * Math::kRadToDeg;
}

//////////////////////////////////////////////////////////////////////////
//  Vector2
//////////////////////////////////////////////////////////////////////////
struct Vec2
{
	float x, y ;

	explicit Vec2( ZeroTag );
	explicit Vec2( OneTag );
	Vec2( float f = 0.0f ) ;
	Vec2( float x, float y ) ;
	Vec2( const Vec3 &v ) ;
	Vec2( const Vec4 &v ) ;
	Vec2 &Set( float x, float y ) ;
	Vec2 &operator=( float f ) ;
	Vec2 &operator=( const Vec3 &v ) ;
	Vec2 &operator=( const Vec4 &v ) ;
	float &operator[]( int num ) ;
	const float &operator[]( int num ) const ;
	bool operator==( const Vec2 &v ) const ;
	bool operator!=( const Vec2 &v ) const ;
	bool operator<( const Vec2 &v ) const ;
} TEA_ALIGNED ;

//////////////////////////////////////////////////////////////////////////
//  Vec3
//////////////////////////////////////////////////////////////////////////
struct Vec3
{
	float x, y, z ;

	explicit Vec3( ZeroTag );
	explicit Vec3( OneTag );
	explicit Vec3( UnitXAxisTag );
	explicit Vec3( UnitYAxisTag );
	explicit Vec3( UnitZAxisTag );
	Vec3( float f = 0.0f ) ;
	Vec3( float x, float y, float z = 0.0f ) ;
	Vec3( const Vec2 &v ) ;
	Vec3( const Vec4 &v ) ;
	Vec3 &Set( float x, float y, float z = 0.0f ) ;
	Vec3 &operator=( float f ) ;
	Vec3 &operator=( const Vec2 &v ) ;
	Vec3 &operator=( const Vec4 &v ) ;
	float &operator[]( int num ) ;
	const float &operator[]( int num ) const ;
	bool operator==( const Vec3 &v ) const ;
	bool operator!=( const Vec3 &v ) const ;
	bool operator<( const Vec3 &v ) const ;
	bool operator<=( const Vec3 &v ) const ;
	bool operator>=( const Vec3 &v ) const ;
} TEA_ALIGNED ;

//////////////////////////////////////////////////////////////////////////
//  Vec4
//////////////////////////////////////////////////////////////////////////
struct Vec4
{
	float x, y, z, w ;

	explicit Vec4( ZeroTag );
	explicit Vec4( OneTag );
	explicit Vec4( UnitXAxisTag );
	explicit Vec4( UnitYAxisTag );
	explicit Vec4( UnitZAxisTag );
	explicit Vec4( UnitWAxisTag );
	explicit Vec4( const Quat &q1 );
	Vec4( float f = 0.0f ) ;
	Vec4( float x, float y, float z = 0.0f, float w = 0.0f ) ;
	Vec4( const Vec2 &v ) ;
	Vec4( const Vec3 &v ) ;
	Vec4 &Set( float x, float y, float z = 0.0f, float w = 0.0f ) ;
	Vec4 &operator=( float f ) ;
	Vec4 &operator=( const Vec2 &v ) ;
	Vec4 &operator=( const Vec3 &v ) ;
	float &operator[]( int num ) ;
	const float &operator[]( int num ) const ;
	bool operator==( const Vec4 &v ) const ;
	bool operator!=( const Vec4 &v ) const ;
	bool operator<( const Vec4 &v ) const ;
} TEA_ALIGNED;


//////////////////////////////////////////////////////////////////////////
//  Mat4
//////////////////////////////////////////////////////////////////////////
struct Mat4
{
	Vec4 x, y, z, w ;

	explicit Mat4( ZeroTag );
	explicit Mat4( IdentityTag );
	explicit Mat4( const Quat &quat);
	explicit Mat4( const Rect &r );
	Mat4( float f = 1.0f ) ;
	Mat4( float xx, float xy, float xz, float xw,
		float yx, float yy, float yz, float yw,
		float zx, float zy, float zz, float zw,
		float wx, float wy, float wz, float ww ) ;
	Mat4( const Vec4 &x, const Vec4 &y, const Vec4 &z, const Vec4 &w ) ;
	Mat4 &Set( float xx, float xy, float xz, float xw,
		float yx, float yy, float yz, float yw,
		float zx, float zy, float zz, float zw,
		float wx, float wy, float wz, float ww ) ;
	Mat4 &Set( const Vec4 &x, const Vec4 &y, const Vec4 &z, const Vec4 &w ) ;
	Mat4 &operator=( float f ) ;
	Vec4 &operator[]( int num ) ;
	const Vec4 &operator[]( int num ) const ;
	bool operator==( const Mat4 &m ) const ;
	bool operator!=( const Mat4 &m ) const ;
	bool operator<( const Mat4 &m ) const ;

	void SetTranslation( float x, float y, float z);
	void SetTranslation( const Vec3 &vec );
} TEA_ALIGNED;


//////////////////////////////////////////////////////////////////////////
//  Quat
//////////////////////////////////////////////////////////////////////////
struct Quat
{
	float x, y, z, w ;

	explicit Quat( ZeroTag );
	explicit Quat( QuatITag );
	explicit Quat( QuatJTag );
	explicit Quat( QuatKTag );
	explicit Quat( IdentityTag );
	explicit Quat( const Mat4 &m );
	Quat( float f = 1.0f ) ;
	Quat( float x, float y, float z, float w ) ;
	Quat &Set( float x, float y, float z, float w ) ;
	Quat &operator=( float f ) ;
	float &operator[]( int num ) ;
	const float &operator[]( int num ) const ;
	bool operator==( const Quat &q ) const ;
	bool operator!=( const Quat &q ) const ;
	bool operator<( const Quat &q ) const ;
} TEA_ALIGNED ;

//////////////////////////////////////////////////////////////////////////
//  Rect
//////////////////////////////////////////////////////////////////////////
struct Rect
{
	float x, y, w, h ;

	explicit Rect( ZeroTag );
	explicit Rect( OneTag );
	explicit Rect( const Mat4 &m );
	Rect( float f=1.0f ) ;
	Rect( float x, float y, float w, float h ) ;
	Rect &Set( float x, float y, float w, float h ) ;
	Rect &operator=( float f ) ;
	float &operator[]( int num ) ;
	const float &operator[]( int num ) const ;
	bool operator==( const Rect &r ) const ;
	bool operator!=( const Rect &r ) const ;
	bool operator<( const Rect &r ) const ;
} ;

//////////////////////////////////////////////////////////////////////////
//  Line funcions
//////////////////////////////////////////////////////////////////////////
struct Line
{
	Vec3	head, tail;

	Line();
	Line( float xa, float ya, float za, float xb, float yb, float zb );
	Line( const Vec3& st, const Vec3& en );
	Line( const Line &other );
	void	operator = (const Line& line);
	bool	operator==( const Line &rhs ) const ;
	bool	operator!=( const Line &rhs ) const ;
};




//////////////////////////////////////////////////////////////////////////
//  axis aligned bounding box
//////////////////////////////////////////////////////////////////////////
struct Aabbox
{
	Vec3	minEdge, maxEdge;

	Aabbox();
	Aabbox( ZeroTag );
	Aabbox( const Vec3& min, const Vec3& max );
	Aabbox( const Vec3& init );
	Aabbox( const Aabbox& other );
	Aabbox( float minx, float miny, float minz, float maxx, float maxy, float maxz );
	bool operator==( const Aabbox &rhs ) const ;
	bool operator!=( const Aabbox &rhs ) const ;
};

//////////////////////////////////////////////////////////////////////////
//  plane functions
//////////////////////////////////////////////////////////////////////////
struct Plane
{
	Vec3	normal;
	float	distance;

	Plane();
	Plane(const Vec3 &norm, float dist);
	Plane(const Vec3 &point, const Vec3 &normal);
	Plane(float px, float py, float pz, float nx, float ny, float nz);
	Plane(const Vec3 &point1, const Vec3 &point2, const Vec3 &point3);
	bool operator==(const Plane& other) const;
	bool operator!=(const Plane& other) const;

};


//////////////////////////////////////////////////////////////////////////
//  frustum functions
//////////////////////////////////////////////////////////////////////////
struct ViewFrustum
{
	enum Planes
	{
		kFarPlane = 0,				///< Far plane of the frustum. Thats the plane farthest away from the eye.
		kNearPlane,					///< Near plane of the frustum. Thats the plane nearest to the eye.
		kLeftPlane,					///< Left plane of the frustum.
		kRightPlane,				///< Right plane of the frustum.	
		kBottomPlane,				///< Bottom plane of the frustum.
		kTopPlane,					///< Top plane of the frustum.
		kPlaneCount					///< Amount of planes enclosing the view frustum
	};
	Vec3	camera_position;			///< camera location
	Plane	planes[ kPlaneCount ];
	Aabbox	bounding_box;

	ViewFrustum();
	ViewFrustum(const Mat4 &mat);
};



void SetCameraPosition(ViewFrustum &frustum, const Vec3 &p);
Aabbox GetBoundingBox( const ViewFrustum &frustum );
Vec3 GetFarLeftUp( const ViewFrustum &frustum );
Vec3 GetFarLeftDown( const ViewFrustum &frustum );
Vec3 GetFarRightUp( const ViewFrustum &frustum );
Vec3 GetFarRightDown( const ViewFrustum &frustum );
void RecalculateBoundingBox( ViewFrustum &frustum );
void TransformFrustum(ViewFrustum &frustum, const Mat4 & mat);






//////////////////////////////////////////////////////////////////////////
//  Plane functions
//////////////////////////////////////////////////////////////////////////

Vec3 GetMemberPoint( const Plane &plane);
float Distance(const Plane &plane, const Vec3 &point);
bool IntersectionExists(const Plane &plane, const Plane &other);
bool GetIntersectionWithLine(const Plane &plane,
							 const Vec3 &linePoint,
							 const Vec3 &lineVect,
							 Vec3 &outIntersection);
float GetKnownIntersectionWithLine( const Plane &plane,
								   const Vec3& linePoint1,
								   const Vec3& linePoint2);
bool GetIntersectionWithLimitedLine(const Plane &plane, 
									const Vec3& linePoint1,
									const Vec3& linePoint2,
									Vec3& outIntersection);
bool GetIntersectionWithPlane(const Plane &plane,
							  const Plane &other,
							  Vec3  &outLinePoint,
							  Vec3  &outLineVect);
bool GetIntersectionWithPlanes(const Plane &plane,
							   const Plane &o1,
							   const Plane &o2,
							   Vec3 &outPoint);
IntersectionRelation ClassifyPointRelation(const Plane &plane, const Vec3& point);
bool IsFrontFacing(const Plane &plane, const Vec3 &lookDirection);


//////////////////////////////////////////////////////////////////////////
//  Vector functions
//////////////////////////////////////////////////////////////////////////
Vec4 operator+( const Vec4 &v1, const Vec4 &v2 ) ;
Vec4 operator-( const Vec4 &v1, const Vec4 &v2 ) ;
Vec4 operator-( const Vec4 &v1 ) ;
Vec4 operator*( const Vec4 &v1, const Vec4 &v2 ) ;
Vec4 operator*( const Vec4 &v1, float f ) ;
Vec4 operator*( float f, const Vec4 &v1 ) ;
Vec4 operator/( const Vec4 &v1, const Vec4 &v2 ) ;
Vec4 operator/( const Vec4 &v1, float f ) ;
Vec4 operator/( float f, const Vec4 &v1 ) ;
Vec4 &operator+=( Vec4 &v1, const Vec4 &v2 ) ;
Vec4 &operator-=( Vec4 &v1, const Vec4 &v2 ) ;
Vec4 &operator*=( Vec4 &v1, const Vec4 &v2 ) ;
Vec4 &operator*=( Vec4 &v1, float f ) ;
Vec4 &operator/=( Vec4 &v1, const Vec4 &v2 ) ;
Vec4 &operator/=( Vec4 &v1, float f ) ;

Vec3 operator+( const Vec3 &v1, const Vec3 &v2 ) ;
Vec3 operator-( const Vec3 &v1, const Vec3 &v2 ) ;
Vec3 operator-( const Vec3 &v1 ) ;
Vec3 operator*( const Vec3 &v1, const Vec3 &v2 ) ;
Vec3 operator*( const Vec3 &v1, float f ) ;
Vec3 operator*( float f, const Vec3 &v1 ) ;
Vec3 operator/( const Vec3 &v1, const Vec3 &v2 ) ;
Vec3 operator/( const Vec3 &v1, float f ) ;
Vec3 operator/( float f, const Vec3 &v1 ) ;
Vec3 &operator+=( Vec3 &v1, const Vec3 &v2 ) ;
Vec3 &operator-=( Vec3 &v1, const Vec3 &v2 ) ;
Vec3 &operator*=( Vec3 &v1, const Vec3 &v2 ) ;
Vec3 &operator*=( Vec3 &v1, float f ) ;
Vec3 &operator/=( Vec3 &v1, const Vec3 &v2 ) ;
Vec3 &operator/=( Vec3 &v1, float f ) ;
Vec3 operator*( const Vec4 &v1, const Vec3 &v2 );

Vec2 operator+( const Vec2 &v1, const Vec2 &v2 ) ;
Vec2 operator-( const Vec2 &v1, const Vec2 &v2 ) ;
Vec2 operator-( const Vec2 &v1 ) ;
Vec2 operator*( const Vec2 &v1, const Vec2 &v2 ) ;
Vec2 operator*( const Vec2 &v1, float f ) ;
Vec2 operator*( float f, const Vec2 &v1 ) ;
Vec2 operator/( const Vec2 &v1, const Vec2 &v2 ) ;
Vec2 operator/( const Vec2 &v1, float f ) ;
Vec2 operator/( float f, const Vec2 &v1 ) ;
Vec2 &operator+=( Vec2 &v1, const Vec2 &v2 ) ;
Vec2 &operator-=( Vec2 &v1, const Vec2 &v2 ) ;
Vec2 &operator*=( Vec2 &v1, const Vec2 &v2 ) ;
Vec2 &operator*=( Vec2 &v1, float f ) ;
Vec2 &operator/=( Vec2 &v1, const Vec2 &v2 ) ;
Vec2 &operator/=( Vec2 &v1, float f ) ;

// Use 2 elements inside a vec4
bool	Equals2( const Vec4 &v1, const Vec4 &v2, float e = 0.0f ) ;
float	Dot2( const Vec4 &v1, const Vec4 &v2 ) ;
float	Length2( const Vec4 &v1 ) ;
Vec4	Normalize2( const Vec4 &v1 ) ;
Vec4	Lerp2( const Vec4 &v1, const Vec4 &v2, float f ) ;
Vec4	Clamp2( const Vec4 &v1, float min, float max ) ;

// Use 3 elements inside a Vec4
bool	Equals3( const Vec4 &v1, const Vec4 &v2, float e = 0.0f ) ;
float	Dot3( const Vec4 &v1, const Vec4 &v2 ) ;
Vec4	Cross3( const Vec4 &v1, const Vec4 &v2 ) ;
float	Length3( const Vec4 &v1 ) ;
Vec4	Normalize3( const Vec4 &v1 ) ;
Vec4	Lerp3( const Vec4 &v1, const Vec4 &v2, float f ) ;
Vec4	Clamp3( const Vec4 &v1, float min, float max ) ;

// Use all 4 elements inside a Vec4
bool	Equals4( const Vec4 &v1, const Vec4 &v2, float e = 0.0f ) ;
float	Dot4( const Vec4 &v1, const Vec4 &v2 ) ;
float	Length4( const Vec4 &v1 ) ;
Vec4	Normalize4( const Vec4 &v1 ) ;
Vec4	Lerp4( const Vec4 &v1, const Vec4 &v2, float f ) ;
Vec4	Clamp4( const Vec4 &v1, float min, float max ) ;

// Uses 3 elements inside a Vec3
float	Distance( const Vec3 &v1, const Vec3 &v2 );
float	DistanceSqr( const Vec3 &v1, const Vec3 &v2 );
bool	Equals( const Vec3 &v1, const Vec3 &v2, float e = 0.0f ) ;
float	Dot( const Vec3 &v1, const Vec3 &v2 ) ;
Vec3	Cross( const Vec3 &v1, const Vec3 &v2 ) ;
float	Length( const Vec3 &v1 ) ;
Vec3	Normalize( const Vec3 &v1 ) ;
Vec3	Lerp( const Vec3 &v1, const Vec3 &v2, float f ) ;
Vec3	Clamp( const Vec3 &v1, float min, float max ) ;
bool	IsBetweenPoints( const Vec3 &point, const Vec3 &begin, const Vec3 &end );

// Uses 2 elements inside a Vec2
float	Dot( const Vec2 &v1, const Vec2 &v2 ) ;


// Line functions
Line	operator + (const Line &line, const Vec3& rhs);
Line	operator - (const Line &line, const Vec3& rhs);
Line&	operator +=(Line &line, const Vec3& rhs);
Line&	operator -=(Line &line, const Vec3& rhs);
float	Length( const Line &line );
float	LengthSqr( const Line &line );
Vec3	GetMiddle( const Line &line );
Vec3	GetVector( const Line &line );
bool	IsPointInBounds( const Line &line, const Vec3& vec);
Vec3	GetClosestPoint( const Line &line, const Vec3& vec);

// Aabbox functions
Aabbox & operator+=( Aabbox &box, const Aabbox &rhs );
Aabbox & operator+=( Aabbox &box, const Vec3 &rhs );
bool	IsPointInside( const Aabbox &box, const Vec3 &p );
bool	IntersectsWithBox( const Aabbox &box, const Aabbox &other );
bool	IntersectsWithLine( const Aabbox &box, const Line &line );
Vec3	GetCentre( const Aabbox &box );
Vec3	GetExtent( const Aabbox &box );
bool	IsEmpty( const Aabbox &box );
Aabbox	GetInterpolated( const Aabbox &box, const Aabbox &other, float d);
bool IntersectsWithLine(const Aabbox &box, const Line& line);
bool IntersectsWithLine(const Aabbox &box,
						const Vec3& linemiddle,
						const Vec3& linevect,
						float halflength);

//----------------------------------------------------------------
//  matrix functions
//----------------------------------------------------------------

Mat4 operator+( const Mat4 &m1, const Mat4 &m2 ) ;
Mat4 operator-( const Mat4 &m1, const Mat4 &m2 ) ;
Mat4 operator-( const Mat4 &m1 ) ;
Mat4 operator*( const Mat4 &m1, const Mat4 &m2 ) ;
Mat4 operator*( const Mat4 &m1, float f ) ;
Mat4 operator*( float f, const Mat4 &m1 ) ;
Vec4 operator*( const Mat4 &m1, const Vec4 &v1 );
Vec3 operator*( const Mat4 &m1, const Vec3 &v1 );
Mat4 operator/( const Mat4 &m1, float f ) ;
Mat4 &operator+=( Mat4 &m1, const Mat4 &m2 ) ;
Mat4 &operator-=( Mat4 &m1, const Mat4 &m2 ) ;
Mat4 &operator*=( Mat4 &m1, const Mat4 &m2 ) ;
Mat4 &operator*=( Mat4 &m1, float f ) ;
Mat4 &operator/=( Mat4 &m1, float f ) ;

bool Equals( const Mat4 &m1, const Mat4 &m2, float e = 0.0f ) ;
bool Equals( const Mat4 &m1, const Mat4 &m2, float e, float ew ) ;
Mat4 Transpose( const Mat4 &m1 ) ;
Mat4 Normalize( const Mat4 &m1 ) ;
Mat4 Inverse( const Mat4 &m1 ) ;
Mat4 InverseOrtho( const Mat4 &m1 ) ;
Mat4 InverseOrthonormal( const Mat4 &m1 ) ;
Mat4 Lerp( const Mat4 &m1, const Mat4 &m2, float f ) ;
Vec4 Transform2( const Mat4 &m1, const Vec4 &v1, float z = 1.0f, float w = 1.0f ) ;
Vec4 Project2( const Mat4 &m1, const Vec4 &v1, float z = 1.0f, float w = 1.0f ) ;
Vec4 Transform3( const Mat4 &m1, const Vec4 &v1, float w = 1.0f ) ;
Vec4 Project3( const Mat4 &m1, const Vec4 &v1, float w = 1.0f ) ;
Vec4 Transform4( const Mat4 &m1, const Vec4 &v1 ) ;
Vec4 Project4( const Mat4 &m1, const Vec4 &v1 ) ;
Vec3 Transform( const Mat4 &m1, const Vec3 &v1 );
Aabbox Transform( const Mat4 &m1, const Aabbox &box );
Mat4 &	Translate( Mat4 &m1, const Vec3 &vec );
Mat4 &	Translate( Mat4 &m1, float x, float y, float z );


namespace Matrix
{
	Mat4 RotateTranslate( const Quat &quat, const Vec4 &t);
	Mat4 RotateTranslate( const Quat &quat, float x, float y, float z);
	Mat4 Translate( const Vec4 &v1 ) ;
	Mat4 Translate( float x, float y, float z ) ;
	Mat4 Rotate( const Vec4 &axis, float angle ) ;
	Mat4 Rotate( float x, float y, float z, float angle ) ;
	Mat4 Rotx( float x ) ;
	Mat4 Roty( float y ) ;
	Mat4 Rotz( float z ) ;
	Mat4 Rotzyx( const Vec4 &angle ) ;
	Mat4 Rotyxz( const Vec4 &angle ) ;
	Mat4 Rotxzy( const Vec4 &angle ) ;
	Mat4 Rotxyz( const Vec4 &angle ) ;
	Mat4 Rotyzx( const Vec4 &angle ) ;
	Mat4 Rotzxy( const Vec4 &angle ) ;
	Mat4 Rotzyx( float x, float y, float z ) ;
	Mat4 Rotyxz( float x, float y, float z ) ;
	Mat4 Rotxzy( float x, float y, float z ) ;
	Mat4 Rotxyz( float x, float y, float z ) ;
	Mat4 Rotyzx( float x, float y, float z ) ;
	Mat4 Rotzxy( float x, float y, float z ) ;
	Mat4 Scale( const Vec4 &scale ) ;
	Mat4 Scale( float x, float y, float z ) ;
	Mat4 Viewport( float x, float y, float w, float h, float z_near = 0.0f, float z_far = 1.0f ) ;
	Mat4 Perspective( float fovy, float aspect, float z_near, float z_far ) ;
	Mat4 Frustum( float left, float right, float bottom, float top, float z_near, float z_far ) ;
	Mat4 Ortho( float left, float right, float bottom, float top, float z_near, float z_far ) ;
	Mat4 Lookat( const Vec4 &eye, const Vec4 &center, const Vec4 &up ) ;
	Mat4 Lookat( float ex, float ey, float ez, float cx, float cy, float cz, float ux, float uy, float uz ) ;
	Mat4 Lookat( const Vec4 &dir ) ;
}


//////////////////////////////////////////////////////////////////////////
//  Quaternion functions
//////////////////////////////////////////////////////////////////////////
Quat operator*( const Quat &q1, const Quat &q2 ) ;
Vec4 operator*( const Quat &q1, const Vec4 &v1 ) ;
Quat &operator*=( Quat &q1, const Quat &q2 ) ;

bool	Equals( const Quat &q1, const Quat &q2, float e = 0.0f ) ;
float	Dot( const Quat &q1, const Quat &q2 ) ;
float	Length( const Quat &q1 ) ;
Quat	Normalize( const Quat &q1 ) ;
Quat	Inverse( const Quat &q1 ) ;
Quat	Slerp( const Quat &q1, const Quat &q2, float f ) ;
Vec4	Transform2( const Quat &q1, const Vec4 &v1, float z = 1.0f, float w = 1.0f ) ;
Vec4	Transform3( const Quat &q1, const Vec4 &v1, float w = 1.0f ) ;
Vec4	Transform4( const Quat &q1, const Vec4 &v1 ) ;

namespace Quaternion
{
	Quat Rotate( const Vec4 &axis, float angle ) ;
	Quat Rotate( float x, float y, float z, float angle ) ;
	Quat Rotzyx( const Vec4 &angle ) ;
	Quat Rotyxz( const Vec4 &angle ) ;
	Quat Rotxzy( const Vec4 &angle ) ;
	Quat Rotxyz( const Vec4 &angle ) ;
	Quat Rotyzx( const Vec4 &angle ) ;
	Quat Rotzxy( const Vec4 &angle ) ;
	Quat Rotzyx( float x, float y, float z ) ;
	Quat Rotyxz( float x, float y, float z ) ;
	Quat Rotxzy( float x, float y, float z ) ;
	Quat Rotxyz( float x, float y, float z ) ;
	Quat Rotyzx( float x, float y, float z ) ;
	Quat Rotzxy( float x, float y, float z ) ;
}

namespace Vector4
{
	Vec4 Rotzyx( const Quat &q1 ) ;
	Vec4 Rotyxz( const Quat &q1 ) ;
}


//////////////////////////////////////////////////////////////////////////
//  Rectangle functions
//////////////////////////////////////////////////////////////////////////
Rect operator*( const Rect &r1, const Rect &r2 ) ;
Vec4 operator*( const Rect &r1, const Vec4 &v1 ) ;
Rect &operator*=( Rect &r1, const Rect &r2 ) ;
Rect operator+( const Rect &r1, const Vec4 &v1 ) ;


bool Equals( const Rect &r1, const Rect &r2, float e = 0.0f ) ;
Rect Inverse( const Rect &r1 ) ;
Rect Lerp( const Rect &r1, const Rect &r2, float f ) ;
Vec4 Transform2( const Rect &q1, const Vec4 &v1, float z = 1.0f, float w = 1.0f ) ;
Vec4 Transform3( const Rect &q1, const Vec4 &v1, float w = 1.0f ) ;
Vec4 Transform4( const Rect &q1, const Vec4 &v1 ) ;


//////////////////////////////////////////////////////////////////////////
//  float functions
//////////////////////////////////////////////////////////////////////////
bool Equals( float f1, float f2, float e = Math::kEpsilon ) ;
bool Equals( const float *f1, const float *f2, int count, float e = Math::kEpsilon ) ;


//////////////////////////////////////////////////////////////////////////
//  Templates
//////////////////////////////////////////////////////////////////////////

template<typename T>
float	Distance( const T &v1, const T &v2 )
{
	T delta = v2 - v1;
	return sqrtf( Dot( delta, delta ) );
}

template<typename T>
float	DistanceSqr( const T &v1, const T &v2 )
{
	T delta = v2 - v1;
	return Dot(delta, delta);
}

template<typename T>
float	Length( const T &v1 )
{
	return sqrtf( Dot( v1, v1 ) ) ;
}

template<typename T>
float	LengthSqr( const T &v1 )
{
	return Dot( v1, v1 );
}


} // tea namespace

#include "tea/math.inl"

