#include <math.h>

namespace tea
{

using namespace Math;

//----------------------------------------------------------------
//  Vec2
//----------------------------------------------------------------
inline Vec2::Vec2( ZeroTag )
{
	Set( 0.0f, 0.0f );
}

inline Vec2::Vec2( OneTag )
{
	Set( 1.0f, 1.0f );
}


inline Vec2::Vec2( float f )
{
	Set( f, f ) ;
}

inline Vec2::Vec2( float x, float y )
{
	Set( x, y ) ;
}

inline Vec2::Vec2( const Vec3 &v )
{
	Set( v.x, v.y ) ;
}

inline Vec2::Vec2( const Vec4 &v )
{
	Set( v.x, v.y ) ;
}

inline Vec2 &Vec2::Set( float x, float y )
{
	this->x = x ;
	this->y = y ;
	return *this ;
}

inline Vec2 &Vec2::operator=( float f )
{
	return Set( f, f ) ;
}

inline Vec2 &Vec2::operator=( const Vec3 &v )
{
	return Set( v.x, v.y ) ;
}

inline Vec2 &Vec2::operator=( const Vec4 &v )
{
	return Set( v.x, v.y ) ;
}

inline float &Vec2::operator[]( int num )
{
	return ( (float *)&x )[ num ] ;
}

inline const float &Vec2::operator[]( int num ) const
{
	return ( (const float *)&x )[ num ] ;
}

inline bool Vec2::operator==( const Vec2 &v ) const
{
	return ( x == v.x && y == v.y ) ;
}

inline bool Vec2::operator!=( const Vec2 &v ) const
{
	return ( x != v.x || y != v.y ) ;
}

inline bool Vec2::operator<( const Vec2 &v ) const
{
	if ( x != v.x ) return ( x < v.x ) ;
	return ( y < v.y ) ;
}

//----------------------------------------------------------------
//  Vec3
//----------------------------------------------------------------
inline Vec3::Vec3( ZeroTag )
{
	Set( 0.0f, 0.0f, 0.0f );
}

inline Vec3::Vec3( OneTag )
{
	Set( 1.0f, 1.0f, 1.0f );
}

inline Vec3::Vec3( UnitXAxisTag )
{
	Set( 1.0f, 0.0f, 0.0f );
}

inline Vec3::Vec3( UnitYAxisTag )
{
	Set( 0.0f, 1.0f, 0.0f );
}

inline Vec3::Vec3( UnitZAxisTag )
{
	Set( 0.0f, 0.0f, 1.0f );
}

inline Vec3::Vec3( float f )
{
	Set( f, f, f ) ;
}

inline Vec3::Vec3( float x, float y, float z )
{
	Set( x, y, z ) ;
}

inline Vec3::Vec3( const Vec2 &v )
{
	Set( v.x, v.y, 1.0f ) ;
}

inline Vec3::Vec3( const Vec4 &v )
{
	Set( v.x, v.y, v.z ) ;
}

inline Vec3 &Vec3::Set( float x, float y, float z )
{
	this->x = x ;
	this->y = y ;
	this->z = z ;
	return *this ;
}

inline Vec3 &Vec3::operator=( float f )
{
	return Set( f, f, f ) ;
}

inline Vec3 &Vec3::operator=( const Vec2 &v )
{
	return Set( v.x, v.y, 1.0f ) ;
}

inline Vec3 &Vec3::operator=( const Vec4 &v )
{
	return Set( v.x, v.y, v.z ) ;
}

inline float &Vec3::operator[]( int num )
{
	return ( (float *)&x )[ num ] ;
}

inline const float &Vec3::operator[]( int num ) const
{
	return ( (const float *)&x )[ num ] ;
}

inline bool Vec3::operator==( const Vec3 &v ) const
{
	return ( x == v.x && y == v.y && z == v.z ) ;
}

inline bool Vec3::operator!=( const Vec3 &v ) const
{
	return ( x != v.x || y != v.y || z != v.z ) ;
}

inline bool Vec3::operator<( const Vec3 &v ) const
{
	if ( x != v.x ) return ( x < v.x ) ;
	if ( y != v.y ) return ( y < v.y ) ;
	return ( z < v.z ) ;
}
inline bool Vec3::operator<=( const Vec3 &v ) const
{
	return x <= v.x && y <= v.y && z <= v.z;
}
inline bool Vec3::operator>=( const Vec3 &v ) const
{
	return x >= v.x && y >= v.y && z >= v.z;
}

//----------------------------------------------------------------
//  Vec4
//----------------------------------------------------------------
inline Vec4::Vec4( ZeroTag )
{
	Set( 0.0f, 0.0f, 0.0f, 0.0f );
}

inline Vec4::Vec4( OneTag )
{
	Set( 1.0f, 1.0f, 1.0f, 1.0f );
}

inline Vec4::Vec4( UnitXAxisTag )
{
	Set( 1.0f, 0.0f, 0.0f, 0.0f );
}

inline Vec4::Vec4( UnitYAxisTag )
{
	Set( 0.0f, 1.0f, 0.0f, 0.0f );
}

inline Vec4::Vec4( UnitZAxisTag )
{
	Set( 0.0f, 0.0f, 1.0f, 0.0f );
}

inline Vec4::Vec4( UnitWAxisTag )
{
	Set( 0.0f, 0.0f, 0.0, 1.0f );
}

inline Vec4::Vec4( const Quat &q1 )
{
	Vec4 v( q1.x, q1.y, q1.z ) ;
	v = Normalize3( v ) ;
	v.w = acosf( q1.w ) * 2.0f ;
	*this = v;
}

inline Vec4::Vec4( float f )
{
	Set( f, f, f, f ) ;
}

inline Vec4::Vec4( float x, float y, float z, float w )
{
	Set( x, y, z, w ) ;
}

inline Vec4::Vec4( const Vec2 &v )
{
	Set( v.x, v.y, 1.0f, 1.0f ) ;
}

inline Vec4::Vec4( const Vec3 &v )
{
	Set( v.x, v.y, v.z, 1.0f ) ;
}

inline Vec4 &Vec4::Set( float x, float y, float z, float w )
{
	this->x = x ;
	this->y = y ;
	this->z = z ;
	this->w = w ;
	return *this ;
}

inline Vec4 &Vec4::operator=( float f )
{
	return Set( f, f, f, f ) ;
}

inline Vec4 &Vec4::operator=( const Vec2 &v )
{
	return Set( v.x, v.y, 1.0f, 1.0f ) ;
}

inline Vec4 &Vec4::operator=( const Vec3 &v )
{
	return Set( v.x, v.y, v.z, 1.0f ) ;
}

inline float &Vec4::operator[]( int num )
{
	return ( (float *)&x )[ num ] ;
}

inline const float &Vec4::operator[]( int num ) const
{
	return ( (const float *)&x )[ num ] ;
}

inline bool Vec4::operator==( const Vec4 &v ) const
{
	return ( x == v.x && y == v.y && z == v.z && w == v.w ) ;
}

inline bool Vec4::operator!=( const Vec4 &v ) const
{
	return ( x != v.x || y != v.y || z != v.z || w != v.w ) ;
}

inline bool Vec4::operator<( const Vec4 &v ) const
{
	if ( x != v.x ) return ( x < v.x ) ;
	if ( y != v.y ) return ( y < v.y ) ;
	if ( z != v.z ) return ( z < v.z ) ;
	return ( w < v.w ) ;
}



//----------------------------------------------------------------
//  Quat
//----------------------------------------------------------------
inline Quat::Quat( ZeroTag )
{
	Set( 0, 0, 0, 0 );
}

inline Quat::Quat( QuatITag )
{
	Set( 1, 0, 0, 0 );
}

inline Quat::Quat( QuatJTag )
{
	Set( 0, 1, 0, 0 );
}

inline Quat::Quat( QuatKTag )
{
	Set( 0, 0, 1, 0 );
}

inline Quat::Quat( IdentityTag )
{
	Set( 0, 0, 0, 1 );
}

inline Quat::Quat( const Mat4 &m1 )
{
	{
		float t = m1.x.x + m1.y.y + m1.z.z + 1.0f ;
		if ( t > 0.01f ) {
			float w = sqrtf( t ) * 0.5f ;
			float r = 0.25f / w ;
			float x = ( m1.y.z - m1.z.y ) * r ;
			float y = ( m1.z.x - m1.x.z ) * r ;
			float z = ( m1.x.y - m1.y.x ) * r ;
			Set( x, y, z, w ) ;
			return;
		}
		if ( m1.x.x > m1.y.y ) {
			if ( m1.x.x > m1.z.z ) {
				float x = sqrtf( 1.0f + m1.x.x - m1.y.y - m1.z.z ) * 0.5f ;
				float r = 0.25f / x ;
				float y = ( m1.y.x + m1.x.y ) * r ;
				float z = ( m1.z.x + m1.x.z ) * r ;
				float w = ( m1.y.z - m1.z.y ) * r ;
				Set( x, y, z, w ) ;
				return;
			}
		} else {
			if ( m1.y.y > m1.z.z ) {
				float y = sqrtf( 1.0f + m1.y.y - m1.z.z - m1.x.x ) * 0.5f ;
				float r = 0.25f / y ;
				float x = ( m1.y.x + m1.x.y ) * r ;
				float z = ( m1.z.y + m1.y.z ) * r ;
				float w = ( m1.z.x - m1.x.z ) * r ;
				Set( x, y, z, w ) ;
				return;
			}
		}
		float z = sqrtf( 1.0f + m1.z.z - m1.x.x - m1.y.y ) * 0.5f ;
		float r = 0.25f / z ;
		float x = ( m1.z.x + m1.x.z ) * r ;
		float y = ( m1.z.y + m1.y.z ) * r ;
		float w = ( m1.x.y - m1.y.x ) * r ;
		Set( x, y, z, w ) ;
	}
}

inline Quat::Quat( float f )
{
	Set( 0, 0, 0, f ) ;
}

inline Quat::Quat( float x, float y, float z, float w )
{
	Set( x, y, z, w ) ;
}

inline Quat &	Quat::Set( float x, float y, float z, float w )
{
	this->x = x ;
	this->y = y ;
	this->z = z ;
	this->w = w ;
	return *this ;
}

inline Quat &	Quat::operator=( float f )
{
	return Set( 0, 0, 0, f ) ;
}

inline float &	Quat::operator[]( int num )
{
	return ( (float *)&x )[ num ] ;
}

inline const float &Quat::operator[]( int num ) const
{
	return ( (const float *)&x )[ num ] ;
}

inline bool	Quat::operator==( const Quat &q ) const
{
	return ( x == q.x && y == q.y && z == q.z && w == q.w ) ;
}

inline bool	Quat::operator!=( const Quat &q ) const
{
	return ( x != q.x || y != q.y || z != q.z || w != q.w ) ;
}

inline bool	Quat::operator<( const Quat &q ) const
{
	if ( x != q.x ) return ( x < q.x ) ;
	if ( y != q.y ) return ( y < q.y ) ;
	if ( z != q.z ) return ( z < q.z ) ;
	return ( w < q.w ) ;
}


//----------------------------------------------------------------
//  Rect
//----------------------------------------------------------------
inline Rect::Rect( ZeroTag )
{
	Set( 0, 0, 0, 0 ) ;
}

inline Rect::Rect( OneTag )
{
	Set( 0, 0, 1, 1 ) ;
}

inline Rect::Rect( const Mat4 &m1 )
{
	Set( m1.w.x, m1.w.y, m1.x.x, m1.y.y ) ;
}

inline Rect::Rect( float f )
{
	Set( 0, 0, f, f ) ;
}

inline Rect::Rect( float x, float y, float z, float w )
{
	Set( x, y, z, w ) ;
}

inline Rect &	Rect::Set( float x, float y, float w, float h )
{
	this->x = x ;
	this->y = y ;
	this->w = w ;
	this->h = h ;
	return *this ;
}

inline Rect &	Rect::operator=( float f )
{
	return Set( 0, 0, f, f ) ;
}

inline float &	Rect::operator[]( int num )
{
	return ( (float *)&x )[ num ] ;
}

inline const	float &Rect::operator[]( int num ) const
{
	return ( (const float *)&x )[ num ] ;
}

inline bool	Rect::operator==( const Rect &r ) const
{
	return ( x == r.x && y == r.y && w == r.w && h == r.h ) ;
}

inline bool	Rect::operator!=( const Rect &r ) const
{
	return ( x != r.x || y != r.y || w != r.w || h != r.h ) ;
}

inline bool	Rect::operator<( const Rect &r ) const
{
	if ( x != r.x ) return ( x < r.x ) ;
	if ( y != r.y ) return ( y < r.y ) ;
	if ( w != r.w ) return ( w < r.w ) ;
	return ( h < r.h ) ;
}

inline Rect	operator+( const Rect &r1, const Vec4 &v1 )
{
	return Rect( r1.x + v1.x, r1.y + v1.y, r1.w, r1.h ) ;
}


/////////////////////////////////////////////////////////////////
// LINE
/////////////////////////////////////////////////////////////////
// Constructors
inline Line::Line(): head(0.0f,0.0f,0.0f), tail(1.0f,1.0f,1.0f)
{
}

inline Line::Line(float xa, float ya, float za, float xb, float yb, float zb) : head(xa, ya, za), tail(xb, yb, zb)
{
}

inline Line::Line(const Vec3& st, const Vec3& en) : head(st), tail(en)
{
}

inline Line::Line(const Line& other) : head(other.head), tail(other.tail)
{
}

// operators
inline void Line::operator = (const Line & rhs)
{
	head	= rhs.head;
	tail	= rhs.tail;
}

inline bool Line::operator == ( const Line& other ) const
{
	return (head==other.head && tail==other.tail) || (tail==other.head && head==other.tail);
}

inline bool Line::operator != ( const Line& other ) const
{
	return !(*this == other);
}


inline Line operator + (const Line &line, const Vec3 & rhs)
{
	return Line(line.head + rhs, line.tail + rhs);
}

inline Line operator - (const Line &line, const Vec3 & rhs)
{
	return Line(line.head - rhs, line.tail - rhs);
}

inline Line& operator += (Line &line, const Vec3 & rhs)
{
	line = line + rhs;
	return line;
}


inline Line& operator -= (Line &line, const Vec3 & rhs)
{
	line = line - rhs;
	return line;
}


// functions
inline float Length( const Line &line )
{
	return Distance( line.head, line.tail );
}

inline float LengthSqr( const Line &line )
{
	return DistanceSqr( line.head, line.tail );
}

inline Vec3 GetMiddle( const Line &line )
{
	return Vec3( (line.head + line.tail) * 0.5f );
}

inline Vec3 GetVector( const Line &line )
{
	return line.tail - line.head;
}

inline bool IsPointInBounds( const Line &line, const Vec3& point )
{
	return IsBetweenPoints(point, line.head, line.tail);
}

inline Vec3 GetClosestPoint( const Line &line, const Vec3& point )
{
	Vec3 c = point - line.head;
	Vec3 v = line.tail - line.head;
	float d = Length(v);
	v /= d;
	float t = Dot(v, c);

	if (t < 0.0f)
		return line.head;
	if (t > d)
		return line.tail;

	v *= t;
	return line.head + v;
}



/////////////////////////////////////////////////////////////////
// AXIS ALIGNED BOUNDING BOX
/////////////////////////////////////////////////////////////////
inline Aabbox::Aabbox():minEdge(-1.f, -1.f, -1.f), maxEdge(1.f, 1.f, 1.f)
{
}
inline Aabbox::Aabbox( ZeroTag ):minEdge(kZero), maxEdge(kZero)
{
}
inline Aabbox::Aabbox( const Vec3& min, const Vec3& max ):minEdge(min), maxEdge(max)
{
}

inline Aabbox::Aabbox( const Vec3& init ) : minEdge(init), maxEdge(init)
{
}

inline Aabbox::Aabbox( const Aabbox& other ) : minEdge(other.minEdge), maxEdge(other.maxEdge)
{
}
inline Aabbox::Aabbox( float minx, float miny, float minz, float maxx, float maxy, float maxz ): minEdge(minx, miny, minz), maxEdge(maxx, maxy, maxz)
{
}
inline bool Aabbox::operator==(const Aabbox& other) const
{
	return (minEdge == other.minEdge && other.maxEdge == maxEdge);
}

inline bool Aabbox::operator!=(const Aabbox& other) const
{
	return !(minEdge == other.minEdge && other.maxEdge == maxEdge);
}

inline Aabbox& operator += (Aabbox &box, const Aabbox & rhs)
{
	box += rhs.minEdge;
	box += rhs.maxEdge;
	return box;
}

inline Aabbox& operator += (Aabbox &box, const Vec3 & rhs)
{
	if (rhs.x > box.maxEdge.x)	box.maxEdge.x = rhs.x;
	if (rhs.y > box.maxEdge.y)	box.maxEdge.y = rhs.y;
	if (rhs.z > box.maxEdge.z)	box.maxEdge.z = rhs.z;
	if (rhs.x < box.minEdge.x)	box.minEdge.x = rhs.x;
	if (rhs.y < box.minEdge.y)	box.minEdge.y = rhs.y;
	if (rhs.z < box.minEdge.z)	box.minEdge.z = rhs.z;
	return box;
}

inline bool	IsPointInside( const Aabbox &box, const Vec3 &p )
{
	return (	p.x >= box.minEdge.x && p.x <= box.maxEdge.x &&
		p.y >= box.minEdge.y && p.y <= box.maxEdge.y &&
		p.z >= box.minEdge.z && p.z <= box.maxEdge.z);
}

inline bool IntersectsWithBox( const Aabbox &box, const Aabbox &other )
{
	return (box.minEdge <= other.maxEdge) && (box.maxEdge >= other.minEdge);
}


inline Vec3	GetCentre( const Aabbox &box )
{
	return (box.minEdge + box.maxEdge)/2.0f;
}

inline Vec3	GetExtent( const Aabbox &box )
{
	return box.maxEdge - box.minEdge;
}
inline bool	IsEmpty( const Aabbox &box )
{
	return box.minEdge == box.maxEdge;
}
inline Aabbox	GetInterpolated( const Aabbox &box, const Aabbox &other, float d )
{
	float inv = 1.0f - d;
	return Aabbox(	(other.minEdge * inv) + (box.minEdge * d),
		(other.maxEdge * inv) + (box.maxEdge * d) );
}

inline bool IntersectsWithLine(const Aabbox &box, const Line& line)
{
	return IntersectsWithLine(
		box,
		GetMiddle( line ),
		Normalize( GetVector( line ) ),
		Length( line ) * 0.5f );

}


//
// Plane
//
inline void CalculateDistance(Plane &plane, const Vec3& point)
{
	plane.distance = - Dot(point, plane.normal);
}

inline Plane::Plane() : normal(0,1,0) 
{
	CalculateDistance( *this, Vec3(kZero) );
}
inline Plane::Plane(const Vec3 &norm, float dist) : normal(norm), distance(dist)
{
}

inline Plane::Plane(const Vec3 &point, const Vec3 &norm ) :normal(norm)
{
	CalculateDistance( *this, point );
}

inline Plane::Plane(float px, float py, float pz, float nx, float ny, float nz) : normal(nx, ny, nz)
{
	CalculateDistance( *this, Vec3(px, py, pz) );
}

inline Plane::Plane(const Vec3 &point1, const Vec3 &point2, const Vec3 &point3)
{
	normal = Normalize( Cross( (point2 - point1), (point3 - point1) ) );
	CalculateDistance( *this, point1);
}
inline bool Plane::operator==(const Plane& other) const
{
	return (normal == other.normal) && (distance == other.distance);
}
inline bool Plane::operator!=(const Plane& other) const
{
	return !((normal == other.normal) && (distance == other.distance));
}

inline Vec3 GetMemberPoint( const Plane &plane)
{
	return plane.normal * ( - plane.distance );
}

inline bool IntersectionExists(const Plane &plane, const Plane &other)
{
	return Length( Cross( other.normal, plane.normal ) ) > kRoundingError;
}

inline float Distance(const Plane &plane, const Vec3 &point)
{
	return Dot(point, plane.normal) + plane.distance;
}


//----------------------------------------------------------------
//  View Frustum
//----------------------------------------------------------------
inline ViewFrustum::ViewFrustum()
{
}

inline void SetCameraPosition(ViewFrustum &frustum, const Vec3 &p)
{
	frustum.camera_position = p;
}

//! returns a bounding box encosing the whole view frustum
inline Aabbox GetBoundingBox( const ViewFrustum &frustum )
{
	return frustum.bounding_box;
}


//----------------------------------------------------------------
// MATRIX
//----------------------------------------------------------------

inline void Mat4::SetTranslation( float x, float y, float z)
{
	w.x = x;
	w.y = y;
	w.z = z;
}
inline void Mat4::SetTranslation( const Vec3 &vec )
{
	w.x = vec.x;
	w.y = vec.y;
	w.z = vec.z;
}

inline Mat4 &	Translate( Mat4 &m1, const Vec3 &vec )
{
	Vec4 v = m1 * vec;
	m1.z += v;
	return m1;
}

inline Mat4 &	Translate( Mat4 &m1, float x, float y, float z )
{
	return Translate( m1, Vec3(x, y, z) );
}

} // ccg namespace
